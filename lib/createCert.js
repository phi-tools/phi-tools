import fs from "fs-extra";
import path from "path";

import selfsigned from "selfsigned";

const {
    ensureDirSync,
    existsSync,
    readFileSync,
    writeFileSync,
    chmodSync
} = fs;

export const createCert = (attributes) => {
    return selfsigned.generate(attributes, {
        algorithm: "sha256",
        days: 30,
        keySize: 2048,
        extensions: [
            {
                name: "keyUsage",
                keyCertSign: true,
                digitalSignature: true,
                nonRepudiation: true,
                keyEncipherment: true,
                dataEncipherment: true
            },
            {
                name: "extKeyUsage",
                serverAuth: true,
                clientAuth: true,
                codeSigning: true,
                timeStamping: true
            },
            {
                name: "subjectAltName",
                altNames: [
                    {
                        // type 2 is DNS
                        type: 2,
                        value: "localhost"
                    },
                    {
                        type: 2,
                        value: "localhost.localdomain"
                    },
                    {
                        type: 2,
                        value: "lvh.me"
                    },
                    {
                        type: 2,
                        value: "*.lvh.me"
                    },
                    {
                        type: 2,
                        value: "[::1]"
                    },
                    {
                        // type 7 is IP
                        type: 7,
                        ip: "127.0.0.1"
                    },
                    {
                        type: 7,
                        ip: "fe80::1"
                    }
                ]
            }
        ]
    });
};

export const getCert = () => {

    const baseDir = path.join(path.resolve(), "ssl");
    ensureDirSync(baseDir);

    const privateDir = path.join(baseDir, "private");
    const certsDir = path.join(baseDir, "certs");
    ensureDirSync(privateDir);
    ensureDirSync(certsDir);

    const keyPath = path.join(privateDir, "private.pem");
    const certPath = path.join(certsDir, "certificate.pem");
    const bundlePath = path.join(privateDir, "bundle.pem");

    const keyExists = existsSync(keyPath);
    const certExists = existsSync(certPath);
    const bundleExists = existsSync(bundlePath);

    if (!keyExists || !certExists || !bundleExists) {
        {
            local: () => {
                // Create a selfsigned cert for local dev
                const attributes = [{ name: "commonName", value: "localhost" }];
                const pems = createCert(attributes);
                writeFileSync(keyPath, pems.private, { encoding: "utf8" });
                writeFileSync(certPath, pems.cert, { encoding: "utf8" });
                writeFileSync(bundlePath, pems.private + pems.cert, { encoding: "utf8" });
            },
            ...(process.env.CI_COMMIT_REF_SLUG ?? {
                [process.env.CI_COMMIT_REF_SLUG]: () => {
                    // If an environment name is provided we should create a certificate
                    // using letsencrypt for that environment.
                    execSync(
                        `certbot certonly -n --agree-tos --email joshua.thomas.bird@gmail.com --dns-route53 -d phi.tools --config-dir ./.ssl --logs-dir ./tmp/letsencrypt --work-dir ./tmp/letsencrypt`,
                        { stdio: "inherit" }
                    );
                    copySync(`./.ssl/live/phi-tools/fullchain.pem`, certPath, { dereference: true });
                    copySync(`./.ssl/live/phi-tools/privkey.pem`, keyPath, { dereference: true });
                }
            })
        }
    }

    chmodSync(keyPath, "664");
    chmodSync(certPath, "664");
    chmodSync(bundlePath, "664");

    return readFileSync(bundlePath);
};

