import { singularize } from "inflected";

export const collate = (object) => {
    const collated = Object.entries(object).reduce((partial, [key, values]) => {
        return Object.entries(values)
            .reduce((partiallyCollated, [index, value]) => {
                return {
                    ...partiallyCollated,
                    [index]: {
                        ...partiallyCollated[index],
                        [singularize(key)]: value
                    }
                };
            }, partial);
    }, {});
    return Array.from({
        ...collated,
        length: Object.keys(collated).length
    });
};
