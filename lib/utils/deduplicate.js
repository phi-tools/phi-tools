export const deduplicate = array => Array.from(new Set(array));
