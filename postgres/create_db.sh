#!/bin/sh

for db in userapi_accounts userapi_devices mediaapi syncapi roomserver signingkeyserver keyserver federationsender appservice naffka; do
    createdb -U postgres -O postgres dendrite_$db
done
