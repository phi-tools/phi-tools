import { createServer } from "https";
import { readFileSync } from "fs";

import express from "express";
import expressGraphql from "express-graphql";
import passport from "passport";


import { schema } from "./lib/types/index.js";


const { graphqlHTTP } = expressGraphql;

const ssl = {
    key: process.env.SSL_PRIVATE_KEY || readFileSync("./ssl/private/private.pem"),
    cert: process.env.SSL_CERTIFICATE || readFileSync("./ssl/certs/certificate.pem")
};


(async () => {

    const app = express();

    app.use("/matrix", (req, res) => {
        console.log("HELLO MATRIX!");
        matrix.publicRooms((error, data) => {
            console.log(error);
            console.log(`Public Rooms: ${JSON.stringify(data)}`);
            res.end("OK");
        })
    });

    app.post(
        "/login",
        passport.authenticate("local", {
            successRedirect: "/feed",
            faliureRedirect: "/login"
        })
    );
    app.use(
        "/graphql",
        graphqlHTTP(async (req, res, params) => {
            return {
                schema,
                graphiql: true,
                context: { req, res, params }
            };
        })
    );

    app.use("/src", express.static("src"));
    app.use("/", express.static("public"));

    const server = createServer(ssl, app);

    server.listen(9000);

})();
