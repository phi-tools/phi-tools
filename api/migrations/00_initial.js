
export const up = async (knex) => {
    await knex.schema.createTable("agreements", (table) => {
        table.comment("An agreement describes the assignments of accountability to entities.");
        table.uuid("id").primary().comment("The UUID of the agreement.");
        table.uuid("agreement_type_id");
    });
    await knex.schema.createTable("agreement_roles", (table) => {
        table.comment("An agreement role describes obligations an entity would responsible for with respect to an agreement");
        table.uuid("id").primary().comment("The UUID of the agreement role.");
    });
    await knex.schema.createTable("agreement_types", (table) => {
        table.comment("An agreement type describes a collection of obligations to be associated with an agreement");
        table.uuid("id").primary().comment("The UUID of the agreement type.");
    });
    await knex.schema.createTable("assignments", (table) => {
        table.comment("An assignment grants an entity a set of permissions as described by a given role for a given record.");
        table.uuid("id").primary().comment("The UUID of the assignment.");
        table.uuid("role_id").comment("The id of the role that the entity has been granted.");
        table.uuid("node_id").comment("The id of the node to grant the entity permissions on.");
    });
    await knex.schema.createTable("document_adjacency", (table) => {
        table.comment("");
        table.uuid("document_id").comment("The UUID of the document that inherits.");
        table.uuid("document_type_id").comment("The UUID of the document that is inherited from.");
    });
    await knex.schema.createTable("documents", (table) => {
        table.comment("");
        table.uuid("id").primary().comment("The UUID of the document.");
        table.string("name");
        table.string("uri");
        table.text("content");
        table.jsonb("data");
    });
    await knex.schema.createTable("entities", (table) => {
        table.comment("An entity describes an embodiment of agency that may be designated accountability.");
        table.uuid("id").primary().comment("The UUID of the entity.");
    });
    await knex.schema.createTable("evidence", (table) => {
        table.comment("Evidence provides a performance with reviewable documentation.");
        table.uuid("id").primary().comment("The UUID of the evidence.");
        table.uuid("document_id").comment("The UUID of the document to be used as evidence.");
        table.uuid("performance_id");
    });
    await knex.schema.createTable("issues", (table) => {
        table.comment("");
        table.uuid("id").primary().comment("The UUID of the issue.");
    });
    await knex.schema.createTable("nodes", (table) => {
        table.comment("");
        table.uuid("id").primary().comment("The UUID of the node.");
    });
    await knex.schema.createTable("obligations", (table) => {
        table.comment("");
        table.uuid("id").primary().comment("The UUID of the obligation.");
        table.uuid("agreement_type_id").references("agreement_types.id");
        table.uuid("obligator_id").references("obligations.id");
    });
    await knex.schema.createTable("organizations", (table) => {
        table.comment("");
        table.uuid("id").primary().comment("The UUID of the organization.");
    });
    await knex.schema.createTable("performances", (table) => {
        table.comment("");
        table.uuid("id").primary().comment("The UUID of the performance.");
    });
    await knex.schema.createTable("performance_types", (table) => {
        table.comment("");
        table.uuid("id").primary().comment("The UUID of the performance type.");
        table.string("name");
    });
    await knex.schema.createTable("permissions", (table) => {
        table.comment("");
        table.uuid("id").primary().comment("The UUID of the permission.");
    });
    await knex.schema.createTable("reactions", (table) => {
        table.comment("");
        table.uuid("id").primary().comment("The UUID of the reaction.");
    });
    await knex.schema.createTable("reviews", (table) => {
        table.comment("");
        table.uuid("id").primary().comment("The UUID of the review.");
    });
    await knex.schema.createTable("roles", (table) => {
        table.comment("");
        table.uuid("id").primary().comment("The UUID of the role.");
    });
    await knex.schema.createTable("users", (table) => {
        table.comment("");
        table.uuid("id").primary().comment("The UUID of the user.");
    });
};

export const down = async (knex) => {
    await knex.schema.dropTable("users");
    await knex.schema.dropTable("roles");
    await knex.schema.dropTable("reviews");
    await knex.schema.dropTable("reactions");
    await knex.schema.dropTable("permissions");
    await knex.schema.dropTable("performance_types");
    await knex.schema.dropTable("performances");
    await knex.schema.dropTable("organizations");
    await knex.schema.dropTable("obligations");
    await knex.schema.dropTable("nodes");
    await knex.schema.dropTable("issues");
    await knex.schema.dropTable("evidence");
    await knex.schema.dropTable("entities");
    await knex.schema.dropTable("documents");
    await knex.schema.dropTable("document_adjacency");
    await knex.schema.dropTable("assignments");
    await knex.schema.dropTable("agreement_types");
    await knex.schema.dropTable("agreement_roles");
    await knex.schema.dropTable("agreements");
};
