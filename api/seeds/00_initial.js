import { insertData } from "../lib/db/insertData.js";

import basicNeeds from "../data/Basic_Needs.js";
import invocationData from "../data/The_Enaction.js";
import healthPledgeData from "../data/Public_Health_Pledge.js";
import animalCrueltyPledgeData from "../data/Animal_Cruelty_Pledge.js";

export const seed = async () => {
    const data = [
        ...basicNeeds,
        ...invocationData,
        ...healthPledgeData,
        ...animalCrueltyPledgeData
    ];
    await insertData(data);
};
