export default {

    development: {
        client: "postgresql",
        connection: {
            host: process.env.DB_HOST || "localhost",
            port: process.env.DB_PORT || 5432,
            user: process.env.DB_USER || "postgres",
            password: process.env.DB_PASSWORD || "password",
            database: process.env.DB_NAME || "postgres"
        },
        pool: {
            min: 0,
            max: 12
        },
        seeds: {
            directory: "./seeds"
        }
    },
    
};

