import { v4 } from "uuid";

export const Role = ({ id, specifier, description }) => {
    if (typeof id === "undefined") {
        id = v4();
    }
    return {
        role: {
            id,
            specifier,
            description
        }
    };
};

export const typeDefs = `
    type Role {
        name: String
    }

    type Query {
        role(
            id: ID,
            specifier: String
        ): Role
        roles: [Role]
    }

    type Mutation {
        addRole(specifier: String!): Role
    }
`;

export const resolvers = {};
