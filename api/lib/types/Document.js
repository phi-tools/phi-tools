import { tableize } from "inflected";

import { maybeOffset, maybeLimit } from "../db/helpers.js";
import { insertData } from "../db/insertData.js";
import { knex } from "../db/knex.js";
import { pipe } from "../utils/pipe.js";

import { Node } from "./Node.js";


export class Document extends Node {

    static async findById({ id }) {
        if (id === "new") return null;
        const data = await knex("documents").where({ id });
        return new Document(data[0]);
    }

    static async all({ offset, limit }) {
        const data = await pipe(knex("documents"))([
            maybeOffset(offset),
            maybeLimit(limit)
        ]);
        return data.map((row) => new this(row));
    }

    constructor(data) {
        super(data);
        if (typeof data.uri === "undefined") {
            data.uri === "";
        }
        this._name = data.name;
        this._uri = data.uri;
        this._content = data.content;
    }
    
    async save() {
        return await insertData([this]);
    }

    get name() {
        return this._name;
    }

    get uri() {
        return this._uri;
    }

    get content() {
        return this._content;
    }

    get normalized() {
        return {
            ...super.normalized,
            document: {
                id: this.id,
                name: this.name,
                uri: this.uri,
                content: this.content,
            }
        };
    }

}

export const typeDefs = `
    input DocumentInput {
        name: String
        documentTypeId: ID
        content: String
    }

    type Document {
        id: ID
        name: String
        kind: String
        content: String
    }

    type Query {
        document(id: ID!): Document
        documents: [Document]
    }

    type Mutation {
        createDocument(document: DocumentInput): Document
        updateDocument(id: ID, document: DocumentInput): Document
        removeDocument(id: ID!): Boolean
    }
`;
    
export const resolvers = {
    Query: {
        document: (root, { id }) => Document.findById(id),
        documents: (root, { limit, offset }) => Document.all({ limit, offset })
    },
    Mutation: {
        createDocument: (root, { document: doc }) => (new Document(doc))
            .save(),
        updateDocument: (root, { id, document: doc }) => Document.findById(id)
            .assign(doc)
            .save()
    }
};
