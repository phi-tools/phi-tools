import { v4 } from "uuid";

import { Entity } from "./Entity.js";

export const Organization = ({ id, name }) => {
    if (typeof id === "undefined") {
        id = v4();
    }
    return {
        ...Entity({ id, name }),
        organization: {
            id
        }
    };
};

export const typeDefs = `
    type Organization {
        name: String
    }

    type Query {
        organizations: [Organization]
    }

    type Mutation {
        addOrganization(name: String!): Organization
    }
`;

export const resolvers = {};
