import { knex } from "../db/knex.js";
import { maybeOffset, maybeLimit } from "../db/helpers.js";
import { pipe } from "../utils/pipe.js";

import { Document } from "./Document.js";


export class PerformanceType extends Document {
    static async all({ offset, limit }) {
        const data = await pipe(
            knex("documents")
                .innerJoin("performance_types", "performance_types.id", "=", "documents.id")

        )([
            maybeOffset(offset),
            maybeLimit(limit)
        ]);
        return data.map((row) => new this(row));
    }
    constructor(data) {
        super(data);
        if (typeof data.uri === "undefined") {
            data.uri === "";
        }
        this._name = data.name;
        this._uri = data.uri;
        this._content = data.content;
        this._preference = data?.preferece ?? "";

    }
    get name() {
        return this._name;
    }
    get uri() {
        return this._uri;
    }
    get content() {
        return this._content;
    }
    get preference() {
        return this._preference;
    }
    get normalized() {
        return {
            ...super.normalized,
            performance_type: {
                id: this.id,
                name: this.name,
            }
        };
    }
}

export const typeDefs = `
    type PerformanceType {
        id: ID
        name: String
        content: String
        preference: Int
    }

    type Query {
        performanceType(id: ID!): PerformanceType
        performanceTypes: [PerformanceType]
    }

    type Mutation {
        addPerformanceType(
            name: String!,
            description: String!
        ): PerformanceType
    }
`;

export const resolvers = {
    Query: {
        performanceType: (root, { id }) => PerformanceType.findById({ id }),
        performanceTypes: (root, { limit, offset }) => PerformanceType.all({ limit, offset })
        
    }
};
