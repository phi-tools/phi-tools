const SPECIAL_INSTANCE_PROPERTY_NAMES = [
    "constructor",
    "prototype",
    "arguments",
    "caller",
    "name",
    "bind",
    "call",
    "apply",
    "toString",
    "length"
];

const not = (fn) => (...args) => !fn(...args);
const isSpecialProp = (prop) => SPECIAL_INSTANCE_PROPERTY_NAMES.includes(prop);
const copyProps = (source, target) => {
    // This function copies all properties and symbols, filtering out some special ones
    const getSourceProp = (prop) => Object.getOwnPropertyDescriptor(source, prop);
    const getTargetProp = (prop) => Object.getOwnPropertyDescriptor(source, prop);
    const copyProp = (prop) => Object.defineProperty(target, prop, getSourceProp(prop));
    Object.getOwnPropertyNames(source)
        .concat(Object.getOwnPropertySymbols(source))
        .filter(not(isSpecialProp))
        .forEach(copyProp);
};

export const Extensible = class {
    static extend(Base=this, Extension=this) {
        if (Object.prototype.isPrototypeOf.call(Base, this)) {
            console.log("is prototype");
            return this;
        }
        const Extended = class extends Base {
            constructor(...args) {
                console.log(Base, Extension)
                super(...args);
                const extension = new Extension(...args);
                copyProps(extension, this);
            }
        };
        copyProps(Extension.prototype, Extended.prototype);
        copyProps(Extension, Extended);
        return Extended;
    }
    static from(Base) {
        return this.extend(Base, this);
    }
    static with(Extension, ...mixins) {
        console.log(mixins);
        if (mixins.length > 0) {
            return this.from(Extension).with(...mixins);
        }
        return this.from(Extension);
    }
    [Symbol.hasInstance](object) {
        return typeof object.with === "function";
    }
    //[Symbol.hasInstance](object) {
    //    return typeof object.with === "function";
    //}
};
