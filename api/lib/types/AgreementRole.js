import { v4 } from "uuid";

export const AgreementRole = ({ id, agreement_type_id, role_id }) => {
    if (typeof id === "undefined") {
        id = v4();
    }
    return {
        agreement_role: {
            id,
            agreement_type_id,
            role_id
        }
    };
};

export const typeDefs = `
    type AgreementRole {
        id: ID!
        roleId: ID!
    }

    type Query {
        agreementRole(id: ID!): AgreementRole
        agreementRoles: [AgreementRole]
    }

    type Mutation {
        addAgreementRole(name: String!): Agreement
        removeAgreementRole(id: ID!): Boolean
    }
`;

export const resolvers = {
    AgreementRole: {
        
    },
    Query: {

    },
    Mutation: {

    }
};

