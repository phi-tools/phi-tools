import { v4 } from "uuid";

import { Node } from "./Node.js";

export const Performance = ({ id, name }) => {
    if (typeof id === "undefined") {
        id = v4();
    }
    return {
        ...Node({ id }),
        performance: {
            id,
            name
        }
    };
};

export const typeDefs = `
    type Performance {
        id: ID!
    }

    type Query {
        performance(id: ID): Performance
        performances: [Performance]
    }

    type Mutation {
        addPerformance(name: String!): Performance
    }
`;
export const resolvers = {};
