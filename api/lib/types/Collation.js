import Inflector from "inflected";
import { deduplicate } from "../utils/deduplicate.js";
import { defined } from "../utils/defined.js";

const { pluralize } = Inflector;

export const keysForItems = (items) => deduplicate(items.map(Object.keys).flat());

export const decollate = (collated) => {
    console.log("To decollate:", collated);
    
    return Object.fromEntries(keysForItems(collated).map((key) => {
        return [
            pluralize(key),
            collated.map(item => item[key]).filter(defined)
        ];
    }));
};

export class Collation {
    constructor(collated) {
        if (Array.isArray(collated)) {
            this._collated = collated;
        } else {
            this._collated = [];
        }
    }
    decollate() {
        return decollate(this._collated);
    }
}
