import { v4 } from "uuid";

import { Document } from "./Document.js";

export class Entity extends Document {

    constructor(data) {
        super(data);
    }

    get id() {
        return this._id;
    }

    get normalize() {
        return {
            ...Document.from(this),
            entity: {
                id: this.id
            }
        };
    }

};

export const typeDefs = `
    type Entity {
        id: ID!
    }

    type Query {
        entity: Entity
        entities: [Entity]
    }

    type Mutation {
        addEntity(id: ID!): Entity
        removeEntity(id: ID!): Boolean
    }
`;

export const resolvers = {};
