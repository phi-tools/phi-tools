import Knex from "knex";
import knexConfiguration from "../../knexfile.js";

export const knex = Knex(knexConfiguration["development"]);
