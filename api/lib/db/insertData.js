import { decollate } from "../types/Collation.js";
import { knex } from "./knex.js";

const BATCH_INSERT_SIZE = 1000;

export const insertData = async (objects) => {
    console.log(objects);
    const dataset = decollate(await Promise.all(objects.map(async object => {
        console.log(object);
        console.log(await object.mint());
        console.log(object.normalized);
        return object.normalized;
    })));
    await knex.transaction(async (transaction) => await Promise.all(
        Object.entries(dataset)
            .map(async ([tableName, data]) => await transaction.batchInsert(
                tableName,
                data,
                BATCH_INSERT_SIZE
            ))
    ));
};

