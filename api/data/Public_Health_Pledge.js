import { AgreementType } from "../lib/types/AgreementType.js";
import { PerformanceType } from "../lib/types/PerformanceType.js";
import { Obligation } from "../lib/types/Obligation.js";

export const agreement = new AgreementType({
    name: "The Public Health Pledge",
    content: `
# The Public Health Pledge

I pledge to do my part to keep our community in good health as I participate in society.

I understand I have a role in the fight against infectious disease including COVID-19 and commit to responsible actions and to being an accountable member of my communities.

I pledge to uphold the  day by demonstrating personal integrity, respecting others, and supporting my the health of life by:

* Following public health guidelines in place to help protect myself and others.
* Protecting myself and others in high-traffic areas of campus or situations where physical distancing isn’t possible to reduce the spread of COVID-19.
* Keeping my hands clean and sanitized while on campus, around Columbia and at home.
* Adhering to capacity limits in social situations to prevent high-spread scenarios.
* Modifying my actions and encouraging others to do the same as guidelines and recommendations adapt over time.

By taking the pledge to keep the University of South Carolina in good health, I accept responsibility for myself and my actions and will do my best to prevent the spread of COVID-19.
`
});

export const performanceTypes = [].map((performanceType) => {
    return new PerformanceType({
        ...performanceType
    });
});

export const obligations = performanceTypes.map((performanceType) => {
    return new Obligation({
        performance_type_id: performanceType.id,
        obligator_id: agreement.id
    });
});

export default [
    agreement,
    ...performanceTypes,
    ...obligations
];
