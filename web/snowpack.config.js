import { readFileSync } from "fs";

const ssl = {
    key: process.env.SSL_PRIVATE_KEY || readFileSync("./ssl/private/private.pem"),
    cert: process.env.SSL_CERTIFICATE || readFileSync("./ssl/certs/certificate.pem")
};

import proxy from "http2-proxy";

export default {
    extends: "@snowpack/app-scripts-react",
    mount: {
        lib: { 
            url: "/lib"
        },
        public: {
            url: "/"
        },
        src: {
            url: "/src",
            static: false, 
            resolve: true
        },
        style: { 
            url: "/style" 
        }
    },
    alias: {
        "@phi": "./lib",
        "@style": "./style"
    },
    plugins: [
        [
            "@snowpack/plugin-sass",
            {
                loadPath: "styles"
            }
        ]
    ],
    routes: [
        {
            src: "/graphql",
            dest: (req, res) => {
                return proxy.web(req, res, {
                    hostname: "api",
                    port: 9000
                });
            }
    
        },
        {
            match: "routes",
            src: ".*",
            dest: "/index.html"
        }
    ],
    buildOptions: {
        watch: true,
        out: "build"
    },
    packageOptions: {
        types: true,
        external: [
            "assert",
            "events",
            "fs",
            "os",
            "path",
            "sort",
            "stream",
            "timers",
            "url",
            "util"
        ]
    },
    devOptions: {
        secure: ssl,
        open: "none"
    }
}
