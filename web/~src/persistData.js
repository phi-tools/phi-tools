import {
    PERSIST_AGREEMENT,
    PERSIST_DOCUMENT
} from "./api/mutations.js";

export const persist = ({ api }) => (persistable) => persistable
    .map(({t, value}) => {
        return ({
            "DOCUMENT": (value) => ({
                mutation: PERSIST_DOCUMENT,
                variables: {
                    id: value.id,
                    document: {
                        content: value.content,
                        name: value.name
                    }
                }
            }),
            "AGREEMENT": (value) => ({
                mutation: PERSIST_AGREEMENT,
                variables: {
                    id: value.id,
                    agreement: {
                        content: value.content,
                        name: value.name
                    }
                }
            })
        })[t](value);
    }) 
    .map(api.mutate)
    .flatten()
    .map((response) => {
        return {
            t: "SET_DATA",
            value: response
        };
    });

