import xs from "xstream";

export const drain = ({
    transition,
    query,
    mutation,
    subscription,
    sidebarVisibility,
    startRouter,
    connectApp,
    currentRoute
}) => {
    return {
        app: xs.merge(
            sidebarVisibility,
            transition,
            connectApp,
            currentRoute,
        ),
        router: xs.merge(
            startRouter,
            transition
        ),
        api: xs.merge(
            query,
            mutation,
            subscription
        )
    }; 
};

