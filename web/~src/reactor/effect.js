import {
    set//
    //operation//,
    //transition
} from "./utils.js";


export const effect = ({
    sidebarVisibility,
    currentRoute,
    targetRoute,
    startingRouter,
    query,
    mutation,
    connectingApp,
    subscription
}) => { 
    return {
        ...set({
            sidebarVisibility,
            currentRoute
        }),
        ...operation({
            query,
            mutation,
            subscription,
            connectApp: connectingApp,
            startRouter: startingRouter
        }),
        ...transition({ transition: targetRoute }),
    };
};

