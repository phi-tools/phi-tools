
const createClickHandler = (router, opts, cb) => {
    const which = (e) => {
        e = e || event;
        return e.which === null ? e.button : e.which;
    };

    const getParams = (href) => {
        var params = {};
        var splitHref = href.split("?");

        if (splitHref[1] && splitHref[1].length) {
            splitHref[1].split("&").forEach(function(param) {
                const i = param.indexOf("=");

                if (i === -1 || i === param.length - 1) {
                    params[decodeURIComponent(param)] = "";
                    return;
                }

                const name = decodeURIComponent(param.substr(0, i));
                const value = decodeURIComponent(param.substr(i + 1));
                params[name] = value;
            });
        }

        return params;
    };

    return (e) => {
        if (which(e) !== 1) return;

        if (e.metaKey || e.ctrlKey || e.shiftKey) return;
        if (e.defaultPrevented) return;


        // ensure link
        var el = e.target;
        while (el && "A" !== el.nodeName) el = el.parentNode;
        if (!el || "A" !== el.nodeName) return;


        // Ignore if tag has
        // 1. "download" attribute
        // 2. rel="external" attribute
        if (el.hasAttribute("download") || el.getAttribute("rel") === "external") return;


        // check target
        if (el.target) return;
        if (!el.href) return;

        const toRouteState = router.matchUrl(el.href);
        if (toRouteState) {
            e.preventDefault();
            const name = toRouteState.name;
            const params = {
                ...getParams(el.href),
                ...toRouteState.params
            };
            const navigate = (opts) => router.navigate(name, params, opts, cb);
            if (typeof opts === "function") {
                navigate(opts(name, params));
            } else {
                navigate(opts);
            }

        }
    };
};

export const upgradeAnchorsPlugin = () => {
    return (router) => {
        const callback = () => {
            console.log("callback");
        }
        const clickEvent = document.ontouchstart ? "touchstart" : "click";
        const clickHandler = createClickHandler(router, {}, callback);
        return {
            onStart() {
                document.addEventListener(clickEvent, clickHandler, false);
            },
            onStop() {
                document.removeEventListener(clickEvent, clickHandler);
            }
        };
    };
};
