import createRouter from "router5";
import browserPlugin from "router5-plugin-browser";

import { routes } from "./routes.js";
import { upgradeAnchorsPlugin } from "./plugins/upgradeAnchors.js";

export const router = createRouter(routes);

router.usePlugin(browserPlugin());
router.usePlugin(upgradeAnchorsPlugin());
router.start();

