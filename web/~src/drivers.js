import { make as makeAppDriver } from "./app/driver.js";
import { make as makeApiDriver } from "./api/driver.js";
import { make as makeRouterDriver } from "./routing/driver.js";

export const drivers = {
    app: makeAppDriver({
        navigation: {
            brand: { href: "/", text: "φ" },
            start: [
                { href: "/obligations", text: "Obligations" },
                { href: "/collections", text: "Collections" },
                { href: "/agreements", text: "Agreements" },
                { href: "/agreement-types", text: "Agreement Types" },
                { href: "/performances", text: "Performances" },
                { href: "/documents", text: "Documents" }
            ],
            end: [
                { href: "/login", text: "login", classNames: ["login"] }
            ]
        }
    }),
    router: makeRouterDriver(),
    api: makeApiDriver()
};
