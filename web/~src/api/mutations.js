import gql from "graphql-tag";

export const PERSIST_AGREEMENT = gql`
    mutation(
        $agreement: AgreementInput,
        $assignments: [AssignmentInput]
    ) {
        persistAgreement(
            agreement: $agreement,
            assignments: $assignments
        ) {
            id
        }
    }
`;

export const PERSIST_AGREEMENT_TYPE = gql`
    mutation(
        $agreementType: AgreementTypeInput,
        $agreementRoles: [AgreementRoleInput]
    ) {
        persistAgreementType(
            agreementType: $agreementType,
            agreementRoles: $agreementRoles
        ) {
            id
        }
    }
`;

export const PERSIST_DOCUMENT = gql`
    mutation(
        $document: DocumentInput,
    ) {
        persistDocument(
            document: $document
        ) {
            id
        }
    }
`;
