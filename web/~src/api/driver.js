import xs from "xstream";
import { ApolloClient } from "@apollo/client";
import { HttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";

export const make = () => {
    const link = new HttpLink({
        uri: "/graphql"
    });
    const client = new ApolloClient({
        cache: new InMemoryCache(),
        link,
        name: "phi-tools",
        version: "1.0"
    });
    return () => {
        return {
            query: ({
                context,
                errorPolicy,
                fetchPolicy,
                query,
                variables
            }) => xs.fromObservable({
                subscribe: (listener) => {
                    (async () => listener.next(await client.query({
                        context,
                        errorPolicy,
                        fetchPolicy,
                        query,
                        variables
                    })))();
                    return {
                        unsubscribe: () => {
                            console.log("UNSUBSCRIBED QUERY STREAM");
                        }
                    };
                },
            }),
            mutate: ({
                awaitRefetchQueries,
                context,
                errorPolicy,
                fetchPolicy,
                mutation,
                optimisticResponse,
                refetchQueries,
                update,
                updateQueries,
                variables
            }) => xs.fromObservable({
                subscribe: (listener) => {
                    (async () => listener.next(await client.mutate({
                        awaitRefetchQueries,
                        context,
                        errorPolicy,
                        fetchPolicy,
                        mutation,
                        optimisticResponse,
                        refetchQueries,
                        update,
                        updateQueries,
                        variables
                    })))();
                    return {
                        unsubscribe: () => {
                            console.log("UNSUBSCRIBED MUTATION STREAM");
                        }
                    };
                }
            })
        };
    };
};

