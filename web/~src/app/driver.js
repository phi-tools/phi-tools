import xs from "xstream";

import App from "@phi/elements/components/App.js";

export const make = (initialState) => (sunk) => {
    const app = App.create(
        initialState
    );
    sunk.subscribe({
        next: ({ t, value }) => {
            console.log({ t, value });
            return {
                SET_CURRENT_ROUTE: (value) => {
                    console.log(value)
                    let route = value;
                    console.log("WILL_SET_CONTENT", route);
                    app.setContentComponent(route);
                },
                OPERATION_CONNECT_APP: () => {
                    document.body.append(app);
                },
                SHOW_SIDEBAR: () => app._sidebar.show(),
                HIDE_SIDEBAR: () => app._sidebar.hide(),
                SET_DATA: (value) => {
                    console.log("SETTING DATA");
                    console.log(value);
                    app._content?.setData?.(value);
                }
            }[t](value);
        }
    });
    return {
        initialized: xs.of(true),
        connected: app.connected,
        contentSet: app.contentSet
    };
};

