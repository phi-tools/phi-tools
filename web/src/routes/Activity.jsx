import React, {
    useState,
    useEffect,
    useCallback
} from "react";

import {
    Route,
    useParams,
    useLocation
} from "react-router-dom";

import {
    useQuery,
    useMutation,
    gql
} from "@apollo/client";


import { UNITS } from "@phi/units.js";
import { RATES } from "@phi/rates.js";
import { SKILL_LEVELS } from "@phi/skillLevels.js";

export const ActivityRoute = ({}) => {

    const { id = "new" } = useParams();
    const location = useLocation(); 
    const queryParams = new URLSearchParams(location.search);

    const currentUser = {
        roles: [
            "EDITOR"
        ]
    }

    const [ availablePreference, setAvailablePreference ] = useState(10);
    const [ allocatedPreference, setAllocatedPreference ] = useState(0);
    const [ skillLevels, setSkillLevels ] = useState(SKILL_LEVELS);
    const [ rates, setRates ] = useState(RATES);
    const [ units, setUnits ] = useState(UNITS);
    const [ activity, setActivity ] = useState({
        id,
        activityId: id === "new" ? queryParams.get("activity_id") : "",
        name: undefined,
        content: undefined,
        preference: 0,
        skillRank: 0,
        scale: 0,
        rate: RATES[0]
    });

    const [ activityModified, setActivityModified ] = useState(false);

    // update/replace with new value

    const updateActivityProperty = (property) => (value) => {
        setActivity({
            ...activity,
            [property]: value
        })
        setActivityModified(true);
    };
    const updateActivityTitle = useCallback((e) => {
        return updateActivityProperty("name")(e.target.value)
    }, [activity]);
    const updateActivityContent = useCallback((e) => {
        return updateActivityProperty("content")(e.target.value)
    }, [activity]);


    // +/- numeric properties

    const raiseActivityPreference = useCallback(() => {
        setAllocatedPreference(allocatedPreference + 1);
        setaActivity({
            ...activity,
            preference: (activity.preference ?? 0) + 1
        });
        setActivityModified(true);
    }, [activity])
    const lowerActivityPreference = useCallback(() => {
        setAllocatedPreference(allocatedPreference - 1);
        setActivity({
            ...activity,
            preference: (activity.preference ?? 0) - 1
        });
        setActivityModified(true);
    }, [activity])


    const addActivitySkillLevel = useCallback(() => {
        setSkillLevels([...skillLevels, ""]);
        setActivityModified(true);
    }, [skillLevels])
    const addActivityUnit = useCallback(() => {
        setUnits({
            ...units,
            [""]: ""
        });
        setActivityModified(true);
    }, [rates])
    const addActivityRate = useCallback(() => {
        setRates([...rates, ""]);
        setActivityModified(true);
    }, [rates])

    const { loading, data } = useQuery(gql`
        query Activities($id: ID!) {
            performanceType: performanceType(id: $id) {
                id
                name
                content
                preference
            }
        }
    `, { 
        variables: {
            id
        }
    });

    const { createActivityLoading, createdActivity } = useMutation(gql`
        mutation CreateActivity(
            $title: String
            $description: String
            $skillLevels: [ID]
            $units: [ID]
            $rates: [ID]
        ) {
            createdActivity: createActivity(
                title: $title
                description: $description
                skillLevels: $skillLevels
                units: $units
                rates: $rates
            ) {
                id
                title
                description
                skillLevels
                units
                rates
            }
        }
    `, {
        variables: {
            ...activity,
        }
    })

    const { saveActivityLoading, savedActivity } = useMutation(gql`
        mutation SaveActivity(
            $id: ID!
            $title: String
            $description: String
            $skillLevels: [ID]
            $units: [ID]
            $rates: [ID]
        ) {
            savedActivity: saveActivity(
                id: $id
                title: $title
                description: $description
                skillLevels: $skillLevels
                units: $units
                rates: $rates
            ) {
                id
                title
                description
                skillLevels
                units
                rates
            }
        }
    `, {
        variables: {
            ...activity,
        }
    })

    const saveActivityCallback = useCallback(() => {
        const { data } = saveActivity({ activity });
        setActivityModified(false);
    }, [activity]);

    useEffect(() => {
        if (loading || id === "new") {
            return;
        }
        setActivity(data?.performanceType)
        return () => {

        }
    }, [loading]);

    if (id !== "new") {
        if (loading) {
            return (<progress className="progress is-primary"></progress>);
        }

        if (!data?.performanceType) {
            return (
                <main>[!!!] Error Loading Data.</main>
            );
        }
    }

    return (<Route>

        {id === "new"
            ? <section
                className="hero is-info"
            >
                <h1 className="title">Creating a new Activity</h1>
                <div className="subtitle">
                    <p>
                        Engaging in activities is a foundational aspect of existence. Things happen because they are done. Even seemingly innocuous things shuch as an apocryphal tree falling over dont really happen unless an observer enages in the observation of it.
                    </p>
                    <p>
                        Activities describe types of things that members of the comunity may have the capability to perform.
                    </p>
                </div>
            </section>
            : ""
        }

    <main>
        <h1>
            <span className="internal-id">{activity?.id ?? "New"}</span>
            {currentUser.roles.includes("EDITOR")
                ?  ( 
                    <label>
                        Title
                        <input
                            className="input"
                            placeholder="Unnamed Activity"
                            value={activity?.name ?? ""}
                            onChange={updateActivityTitle}
                        />
                    </label>
                )
                : (activity?.name ?? "Unnamed Activity")
            }
        </h1>
        {currentUser.roles.includes("EDITOR")
            ?  ( 
                <label>
                    Description
                    <textarea
                        className="textarea"
                        placeholder="Describe the activity"
                        value={activity?.content ?? ""}
                        onChange={updateActivityContent}
                    />
                </label>
            )
            : (<p>{activity?.content}</p>)
        }

        <h2>Skill Levels</h2>
        <p>
            Describe the skill levels that a person may have the capability to perform this activity at.
        </p>
        <ol
        >
            {skillLevels.map((skillLevel, i) => {
                return (
                    <li
                        style={{
                            marginBottom: "5px",
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            gap: "5px"
                        }}
                    >
                        <span
                            style={{
                                flex: "0 0 20px"
                            }}
                        >
                            {i}.
                        </span>
                        <input
                            className="input is-small"
                            value={skillLevel}
                        />
                        <button
                            className="button is-small is-danger"
                            onClick={() => {
                                setSkillLevels(skillLevels.splice(i, 1));
                                setActivityModified(true);
                            }}
                        >
                            Remove
                        </button>
                    </li>
                );
            })}
        </ol>
        <div
            className="buttons is-right"
        >
            <button 
                className="button is-info"
                onClick={addActivitySkillLevel}
            >
                Add Another Skill Level
            </button>
        </div>

        <h2>Units</h2>
        <p>
            Describe the how the activity can be measured. 
        </p>
        <ol
        >
            {Object.keys(units).map((unit, i) => {
                return (
                    <li
                        style={{
                            marginBottom: "5px",
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            gap: "5px"
                        }}
                    >
                        <span
                            style={{
                                flex: "0 0 20px"
                            }}
                        >
                            {i}.
                        </span>
                        <input
                            className="input is-small"
                            value={unit}
                        />
                        <button
                            className="button is-small is-danger"
                            onClick={() => {
                                setUnits(units.splice(i, 1));
                                setActivityModified(true);
                            }}
                        >
                            Remove
                        </button>
                    </li>
                );
            })}
        </ol>
        <div
            className="buttons is-right"
        >
            <button 
                className="button is-info"
                onClick={addActivityUnit}
            >
                Add Another Rate
            </button>
        </div>

        <div
            class="buttons is-right"
        >
            <button
                className="button is-success is-right"
                disabled={!activityModified}
                onClick={saveActivity}
            >
                Save Changes
            </button>
        </div>

        </main>
    </Route>);

}
