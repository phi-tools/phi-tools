import React, {
    useState,
    useEffect,
    useCallback
} from "react";

import {
    Route,
    useParams,
    useLocation,
    useHistory
} from "react-router-dom";

import {
    useQuery,
    useMutation,
    gql
} from "@apollo/client";

import {
    ChevronDownIcon,
    PlusIcon,
    DashIcon
} from "@primer/octicons-react";

import { Capability } from "../Capability.js";

import { SKILL_LEVELS } from "@phi/skillLevels.js";
import { RATES } from "@phi/rates.js";

const debug = false;

export const CapabilityRoute = ({}) => {

    const history = useHistory(); 
    const { id = "new" } = useParams();
    const location = useLocation(); 
    const queryParams = new URLSearchParams(location.search);
    const activityId = queryParams.get("activity_id") ?? undefined;

    const currentUser = {
        roles: [
            "EDITOR"
        ]
    }
    
    const [ activityChooserActive, setActivityChooserActive ] = useState(false);
    const [ rateChooserActive, setRateChooserActive ] = useState(false);
    const [ availablePreference, setAvailablePreference ] = useState(10);
    const [ allocatedPreference, setAllocatedPreference ] = useState(0);
    const [ activities, setActivities ] = useState([]);
    const [ activity, setActivity ] = useState({
        id: queryParams.get("activity_id") ?? undefined,
        name: undefined,
        content: undefined,
        preference: 0,
        skillRank: 0,
        scale: 0,
        rate: RATES[0]
    });
    const [ capability, setCapability ] = useState(id === "new" 
        ? {
            id,
            activityId: queryParams.get("activity_id") ?? undefined,
            name: undefined,
            content: undefined,
            preference: 0,
            skillRank: 0,
            scale: 0,
            rate: RATES[0]
        }
        : null
    );
    const [ capabilityModified, setCapabilityModified ] = useState(false);

    const { loading, data } = useQuery(gql`
        query Capabilities($id: ID!) {
            performanceType: performanceType(id: $id) {
                id
                name
                content
                preference
            }
            performanceTypes: performanceTypes {
                id
                name
                content
            }
        }
    `, { 
        variables: {
            id: activity.id ?? "new"
        }
    });

    // update/replace with new value

    const updateCapabilityProperty = (property) => (value) => {
        setCapability({
            ...capability,
            [property]: value
        })
        setCapabilityModified(true);
    };
    const updateCapabilityActivityId = useCallback((e) => {
        return updateCapabilityProperty("name")(e.target.value);
    }, [capability]);
    const updateCapabilityTitle = useCallback((e) => {
        return updateCapabilityProperty("name")(e.target.value);
    }, [capability]);
    const updateCapabilityRate = useCallback((value) => {
        return updateCapabilityProperty("rate")(value);
    }, [capability]);
    const updateCapabilityContent = useCallback((e) => {
        return updateCapabilityProperty("content")(e.target.value);
    }, [capability]);


    // +/- numeric properties

    const raiseCapabilityPreference = useCallback(() => {
        setAllocatedPreference(allocatedPreference + 1);
        setCapability({
            ...capability,
            preference: (capability.preference ?? 0) + 1
        });
        setCapabilityModified(true);
    }, [capability])
    const lowerCapabilityPreference = useCallback(() => {
        setAllocatedPreference(allocatedPreference - 1);
        setCapability({
            ...capability,
            preference: (capability.preference ?? 1) - 1
        });
        setCapabilityModified(true);
    }, [capability])
    const raiseCapabilitySkill = useCallback(() => {
        return updateCapabilityProperty("skillRank")(Math.min((capability.skillRank ?? 0) + 1, SKILL_LEVELS.length - 1))
    }, [capability])
    const lowerCapabilitySkill = useCallback(() => {
        return updateCapabilityProperty("skillRank")(Math.max(capability.skillRank - 1, 0))
    }, [capability])

    useEffect(() => {
        if (loading) {
            return;
        }
        console.log({data});
        setCapability(data?.performanceType ?? capability);
        setActivity(data?.performanceType ?? activity);
        setActivities(data?.performanceTypes)
        return () => {

        }
    }, [loading]);

    useEffect(() => {
        if (typeof activity.id !== "undefined") {
            history.replace(`/capabilities/new?activity_id=${activity.id}`);
        }
    }, [activity]);

    useEffect(() => {
        const resetDropdowns = () => {
            setRateChooserActive(false);
        };

        document.addEventListener("click", resetDropdowns);
        return () => {
            document.removeEventListener("click", resetDropdowns);
        }
    }, [])

    if (loading) {
        return (<progress className="progress is-primary"></progress>);
    }

    console.log({
        activityId,
        rateChooserActive

    })

    return (<Route path="/capabilities/:id">


        {id === "new"
            ? 
                <section className="hero is-info"
            >
                <h1 className="title">Creating a new Capability</h1>
                <div className="subtitle">
                    <p>Everyone has different capablilities. The types of activities we perform might the same, but each of us has different preferences and level's of skill at performing these activities.</p>
                    <p>Capability allows the rest of the community to understand an individual&apos;s ability to participate in the performance of a task, and contextualize their desire to perform the given activity.</p>
                </div>
            </section>
            : ""
        }

        <main>

            <Capability
                capability={capability}
            />
        </main>
    </Route>);

}
