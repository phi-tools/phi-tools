import React, {
    useState,
    useEffect,
    useCallback
} from "react";

import {
    useParams,
    useLocation
} from "react-router-dom";

import {
    useQuery,
    useMutation,
    gql
} from "@apollo/client";

import { SKILL_LEVELS } from "@phi/skillLevels.js";
import { RATES } from "@phi/rates.js";

export const Activity = ({}) => {

    const { id = "new" } = useParams();
    const location = useLocation(); 
    const searchParams = new URLSearchParams(location);

    const currentUser = {
        roles: [
            "EDITOR"
        ]
    }

    const [ availablePreference, setAvailablePreference ] = useState(10);
    const [ allocatedPreference, setAllocatedPreference ] = useState(0);
    const [ capability, setCapability ] = useState(id === "new" 
        ? {
            id,
            activityId: queryParams.get("activity_id"),
            name: undefined,
            content: undefined,
            preference: 0,
            skillRank: 0,
            scale: 0,
            rate: RATES[0]
        }
        : null
    );
    const [ capabilityModified, setCapabilityModified ] = useState(false);

    // update/replace with new value

    const updateCapabilityProperty = (property) => (value) => {
        setCapability({
            ...capability,
            [property]: value
        })
        setCapabilityModified(true);
    };
    const updateCapabilityTitle = useCallback((e) => {
        return updateCapabilityProperty("name")(e.target.value)
    }, [capability]);
    const updateCapabilityContent = useCallback((e) => {
        return updateCapabilityProperty("content")(e.target.value)
    }, [capability]);


    // +/- numeric properties

    const raiseCapabilityPreference = useCallback(() => {
        setAllocatedPreference(allocatedPreference + 1);
        setCapability({
            ...capability,
            preference: (capability.preference ?? 0) + 1
        });
        setCapabilityModified(true);
    }, [capability])
    const lowerCapabilityPreference = useCallback(() => {
        setAllocatedPreference(allocatedPreference - 1);
        setCapability({
            ...capability,
            preference: (capability.preference ?? 0) - 1
        });
        setCapabilityModified(true);
    }, [capability])
    const raiseCapabilitySkill = useCallback(() => {
        return updateCapabilityProperty("skillRank")(Math.min(capability.skillRank + 1, SKILL_LEVELS.length - 1))
    }, [capability])
    const lowerCapabilitySkill = useCallback(() => {
        return updateCapabilityProperty("skillRank")(Math.max(capability.skillRank - 1, 0))
    }, [capability])

    const { loading, data } = useQuery(gql`
        query Capabilities($id: ID!) {
            performanceType: performanceType(id: $id) {
                id
                name
                content
                preference
            }
        }
    `, { 
        variables: {
            id
        }
    });

    useEffect(() => {
        if (loading || id === "new") {1
            return;
        }
        setCapability(data?.performanceType)
        return () => {

        }
    }, [loading]);

    if (id !== "new") {
        if (loading) {
            return (<progress className="progress is-primary"></progress>);
        }

        if (!data?.performanceType) {
            return (
                <main>[!!!] Error Loading Data.</main>
            );
        }
    }

    return (<main>

        {id === "new"
            ? <div
                className="notification is-info"
            >
                <h1>Creating a new Capability</h1>
                <p>Everyone has different capablilities. The types of activities we perform miht the same, but each of us has different preferences and level's of skill at performing these activities.</p>
                <p>Capability allows the rest of the community to understand an individual&apos;s ability to participate in the performance of a task, and contextualize their desire to perform the given activity.</p>

            </div>
            : ""
        }

        <h1>
            <span className="internal-id">{capability?.id ?? "New"}</span>
            {currentUser.roles.includes("EDITOR")
                ?  ( 
                    <label>
                        Title
                        <input
                            className="input"
                            placeholder="Unnamed Performance Type"
                            value={capability?.name ?? ""}
                            onChange={updateCapabilityTitle}
                        />
                    </label>
                )
                : (capability?.name ?? "Unnamed Performance Type")
            }
        </h1>
            {currentUser.roles.includes("EDITOR")
                ?  ( 
                    <label>
                        Description
                        <textarea
                            className="textarea"
                            placeholder="Describe the capability"
                            value={capability?.content ?? ""}
                            onChange={updateCapabilityContent}
                        />
                    </label>
                )
                : (<p>{capability?.content}</p>)
            }
        <br/>
        <h2>Preference</h2>
        <div
            className="buttons"
        >

            <button
                className="button is-success"
                disabled={allocatedPreference <= 0 || capability.preference <= 0}
                onClick={lowerCapabilityPreference}
            >
                -
            </button>
            
            <button 
                className="button is-success"
                disabled={allocatedPreference >= availablePreference}
                onClick={raiseCapabilityPreference}
            >
                +
            </button>
            {capability.preference}
        </div>
        <br/>
        <h2>Skill Level</h2>
        <div
            className="buttons"
        >

            <button
                className="button is-success"
                disabled={capability.skillRank <= 0}
                onClick={lowerCapabilitySkill}
            >
                -
            </button>
            
            <button 
                className="button is-success"
                disabled={capability.skillRank >= SKILL_LEVELS.length - 1}
                onClick={raiseCapabilitySkill}
            >
                +
            </button>
            {SKILL_LEVELS[capability.skillRank]}
        </div>

        <br />

        <div class="select">
            <select>
                {RATES.map((rate) => {
                    return (
                        <option
                            value={rate}
                        >
                            {rate}
                        </option>
                    );
                })}
            </select>
        </div>

        <div
            class="buttons is-right"
        >
            <button
                className="button is-success is-right"
                disabled={!capabilityModified}
                onClick={() => {
                    alert("saved!");
                }}
            >
                Save Changes
            </button>
        </div>

    </main>);

}
