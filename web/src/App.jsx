import React, {
    useState
} from "react";

import {
    Switch,
    Route,
    useLocation
} from "react-router-dom";

import { Index } from "./Index.js";
import { Needs } from "./Needs.js";
import { Activities } from "./Activities.js";
import { Agreements } from "./Agreements.js";
import { Agreement } from "./Agreement.js";
import { Obligations } from "./Obligations.js";
import { Capabilities } from "./Capabilities.js";
import { CapabilityRoute } from "./routes/Capability.js";
import { ActivityRoute } from "./routes/Activity.js";
import { Header } from "./Header.js";
import { Nav } from "./Nav.js";
import { Footer } from "./Footer.js";

export const App = () => {

    const location = useLocation();

    const [ navMinimized, setNavMinimized ] = useState(false);

    console.log(location.pathname) ;

    return (<>
        <Header />
        <Nav
            enabled={![
                "/"
            ].includes(location.pathname)}
            minimized={navMinimized}
            setMinimized={setNavMinimized}
        />
        <div class="page">
            <Switch>
                <Route exact path="/">
                    <Index />
                </Route>
                <Route exact path="/activities">
                    <Activities />
                </Route>
                <ActivityRoute path="/activities/:id"/>
                <Route exact path="/agreements">
                    <Agreements />
                </Route>
                <Route path="/agreements/:id">
                    <Agreement />
                </Route>
                <Route path="/obligations">
                    <Obligations />
                </Route>
                <Route path="/needs">
                    <Needs />
                </Route>
                <Route exact path="/capabilities">
                    <Capabilities />
                </Route>
                <CapabilityRoute path="/capabilities/:id"/>
            </Switch>
        </div>
        <Footer />
    </>);
}
