import React from "react";

import {
    Link
} from "react-router-dom"

export const Header = () => {
    return (
        <header>
            <div
                class="brand"
            >
                <Link
                    to="/"
                >
                    &phi;.tools
                </Link>
            </div>
            <div
                class="end"
            >

            </div>
        </header>
    );
}
