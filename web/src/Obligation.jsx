import React, {
    useState,
    useEffect,
    useCallback
} from "react";

import {
    useParams,
    useLocation,
    useHistory
} from "react-router-dom";

import {
    useQuery,
    useMutation,
    gql
} from "@apollo/client";

import {
    TriangleDownIcon,
    PlusIcon,
    DashIcon
} from "@primer/octicons-react";

import { UNITS } from "@phi/units.js";
import { QUALITIES as QUALITY_LEVELS } from "@phi/qualities.js";
import { RATES } from "@phi/rates.js";

const allParties = [{
    name: "Jane Doe",
    role: "executor"
}, {
    name: "Alice",
    role: "promisor"
}, {
    name: "Bob",
    role: "promisor"
}, {
    name: "Charlie",
    role: "promisor"
}, {
    name: "Xenon",
    role: "promisee"
}, {
    name: "Yolanda",
    role: "promisee"
}, {
    name: "Zedd",
    role: "promisee"
}]


const debug = false;

export const Obligation = ({
    save = false,
    remove = false
}) => {

    const history = useHistory(); 
    const { id = "new" } = useParams();
    const location = useLocation(); 
    const queryParams = new URLSearchParams(location.search);
    const activityId = queryParams.get("activity_id") ?? undefined;

    const currentUser = {
        roles: [
            "EDITOR"
        ]
    }
    
    const [ activityChooserActive, setActivityChooserActive ] = useState(false);
    const [ rateChooserActive, setRateChooserActive ] = useState(false);
    const [ unitChooserActive, setUnitChooserActive ] = useState(false);
    const [ availablePreference, setAvailablePreference ] = useState(10);
    const [ allocatedPreference, setAllocatedPreference ] = useState(0);
    const [ activities, setActivities ] = useState([]);
    const [ criteria, setCriteria ] = useState([]);
    const [ selectedParties, setSelectedParties ] = useState([]);

    const [ activity, setActivity ] = useState({
        id: queryParams.get("activity_id") ?? undefined,
        name: undefined,
        content: undefined,
        preference: 0,
        quality: 0,
        scale: 0,
        rate: RATES[0]
    });
    const [ obligation, setObligation ] = useState(id === "new" 
        ? {
            id,
            activityId: queryParams.get("activity_id") ?? undefined,
            name: undefined,
            content: undefined,
            preference: 0,
            quality: 0,
            scale: 0,
            requestedUnits: 0,
            rate: RATES[0]
        }
        : null
    );
    const [ obligationModified, setObligationModified ] = useState(false);

    const { loading, data } = useQuery(gql`
        query Obligations($id: ID!) {
            performanceType: performanceType(id: $id) {
                id
                name
                content
                preference
            }
            performanceTypes: performanceTypes {
                id
                name
                content
            }
        }
    `, { 
        variables: {
            id: activity.id ?? "new"
        }
    });

    // update/replace with new value

    const updateObligationProperty = (property) => (value) => {
        setObligation({
            ...obligation,
            [property]: value
        })
        setObligationModified(true);
    };
    const updateObligationActivityId = useCallback((e) => {
        return updateObligatoinProperty("name")(e.target.value);
    }, [obligation]);
    const updateObligationTitle = useCallback((e) => {
        return updateObligationProperty("name")(e.target.value);
    }, [obligation]);
    const updateObligationRate = useCallback((value) => {
        return updateObligationProperty("rate")(value);
    }, [obligation]);
    const updateObligationUnit = useCallback((value) => {
        return updateObligationProperty("rate")(value);
    }, [obligation]);
    const updateObligationContent = useCallback((e) => {
        return updateObligationProperty("content")(e.target.value);
    }, [obligation]);


    // +/- numeric properties

    const raiseRequestedUnits = useCallback(() => {
        setObligation({
            ...obligation,
            preference: (obligation.preference ?? 0) + 1
        });
        setObligationModified(true);
    }, [obligation])
    const lowerRequestedUnits = useCallback(() => {
        setObligation({
            ...obligation,
            preference: (obligation.preference ?? 1) - 1
        });
        setObligationModified(true);
    }, [obligation])
    const raiseObligationPreference = useCallback(() => {
        setAllocatedPreference(allocatedPreference + 1);
        setObligation({
            ...obligation,
            preference: (obligation.preference ?? 0) + 1
        });
        setObligationModified(true);
    }, [obligation])
    const lowerObligationPreference = useCallback(() => {
        setAllocatedPreference(allocatedPreference - 1);
        setObligation({
            ...obligation,
            preference: (obligation.preference ?? 1) - 1
        });
        setObligationModified(true);
    }, [obligation])
    const raiseObligationQuality = useCallback(() => {
        return updateObligationProperty("quality")(Math.min((obligation.quality ?? 0) + 1, QUALITY_LEVELS.length - 1))
    }, [obligation])
    const lowerObligationQuality = useCallback(() => {
        return updateObligationProperty("quality")(Math.max(obligation.quality - 1, 0))
    }, [obligation])

    useEffect(() => {
        if (loading) {
            return;
        }
        console.log({data});
        setObligation(data?.performanceType ?? obligation);
        setActivity(data?.performanceType ?? activity);
        setActivities(data?.performanceTypes)
        return () => {

        }
    }, [loading]);

    useEffect(() => {
        const resetDropdowns = () => {
            setRateChooserActive(false);
            setUnitChooserActive(false);
        };

        document.addEventListener("click", resetDropdowns);
        return () => {
            document.removeEventListener("click", resetDropdowns);
        }
    }, [])

    if (loading) {
        return (<progress className="progress is-primary"></progress>);
    }

    console.log({
        activityId,
        rateChooserActive

    })

    return (<>
        <div 
            className="box media"
        >
            <div 
                className="media-content"
            >

                {typeof activity.id === "undefined"
                    ? (<>
                        <h1
                            style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center"
                            }}
                        >
                            <span
                                style={{
                                    flex: "1 0 auto"
                                }}
                            >
                                Select an Activity
                            </span>
                            {typeof remove === "function"
                                ? (
                                    <button
                                        className="button is-danger is-light is-right"
                                        onClick={remove}
                                    >
                                        Cancel
                                    </button>
                                ) : ""
                            }
                        </h1>
                        <input
                            className="input"
                            placeholder="Search for an Activity"
                            onFocus={() => setActivityChooserActive(true)}
                            onBlur={() => setActivityChooserActive(false)}
                        />
                        {activities.slice(0, 5).map(activity => {
                            return (
                                <div
                                    className="media"
                                >
                                    <div
                                        className="media-content"
                                    >
                                        <h2>{activity.name}</h2>
                                        <p>{activity.content}</p>
                                    </div>
                                        <div className="buttons is-right">
                                            <button
                                                className="button is-success"
                                                onClick={() => {
                                                    setActivity(activity);
                                                }}
                                            >
                                                Select
                                            </button>
                                        </div>
                                </div>
                            );
                        })}
                        <div
                            className="media pagination is-centered"
                        >
                            <a className="pagination-previous">&lt;</a>
                            <a className="pagination-next">&gt;</a>
                            <ul className="pagination-list">
                                <li className="pagination-link">1</li>
                                <li className="pagination-link">2</li>
                                <li className="pagination-link">3</li>
                            </ul>
                            
                        </div>
                    </>)
                    : (<>
                        <div
                            className="media-content"
                        >
                            <h1
                                style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center"
                                }}
                            >
                                <span
                                    style={{
                                        flex: "1 0 auto"
                                    }}
                                >
                                    {activity.name}
                                </span>
                                {typeof remove === "function"
                                    ? (
                                        <button
                                            className="button is-danger is-right"
                                            onClick={remove}
                                        >
                                            Remove
                                        </button>
                                    ) : ""
                                }
                        </h1>
                            <p>{activity.content}</p>
                        </div>
                    </>)
                }

        {typeof activity.id === "undefined"
            ? ""    
            : (<>
                        <div className="media">
                            <div className="media-content">
                                <h2>Memo</h2>
                                <div className="message is-info">
                                    <div className="message-body">
                                        <p>
                                            When looking at an agreement, there might be more than one obligation for assigned parties to perform. Including a memo should help participants find the information they&apos;re looking for within an agreement quickly.
                                        </p>
                                    </div>
                                </div>
                                <textarea
                                    className="textarea"

                                />
                            </div>

                        </div>

                        <div className="media">
                            <div className="media-content">
                                <h2>Rate</h2>
                                {{
                                    "STATIC": "A static rate describes a finite count of a resource, or the flow of the resource is unknown. Set the scale property to the value capably of being provided",
                                    "YEARLY": "The rate at which this obligation shall be performed is measured on a yearly basis",
                                    "QUARTERLY": "The rate at which this obligation shall be performed is measured on a quarterly basis",
                                    "MONTHLY": "The rate at which this obligation shall be performed is measured on a monthly basis",
                                    "WEEKLY": "The rate at which this obligation shall be performed is measured on a weekly basis",
                                    "DAILY": "The rate at which this obligation shall be performed is measured on a daily basis",
                                    "HOURLY": "The rate at which this obligation shall be performed is measured on a hourly basis"
                                }[obligation.rate ?? RATES[0]]}
                            </div>
                            <div
                                className={`dropdown is-right ${rateChooserActive ? "is-active" : ""}`}
                            >
                                <button
                                    className="dropdown-trigger button is-info"
                                    onClick={(e) => {
                                        console.log("clicked dropdown")
                                        setRateChooserActive(true);
                                        e.preventDefault();
                                        e.stopPropagation();
                                        return false;
                                    }}
                                >
                                    <span>
                                        {obligation.rate ?? RATES[0]}
                                    </span>
                                    <TriangleDownIcon className="icon"/>
                                </button>
                                <div
                                    className="dropdown-menu is-hoverable"
                                >
                                    <div
                                        className="dropdown-content"
                                    >
                                        {RATES.map((rate) => {
                                            return (
                                                <button
                                                    className="dropdown-item"
                                                    onClick={(e) => {
                                                        console.log("Updating");
                                                        updateObligationRate(rate)
                                                    }}
                                                >
                                                    {rate}
                                                </button>
                                            );
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="media">
                            <div className="media-content">
                                <h2>Unit</h2>
                                <p>Define the unit of measure the requested resource shall be measured in.</p>
                            </div>
                            <div
                                className={`dropdown is-right ${unitChooserActive ? "is-active" : ""}`}
                            >
                                <button
                                    className="dropdown-trigger button is-info"
                                    onClick={(e) => {
                                        console.log("clicked dropdown")
                                        setUnitChooserActive(true);
                                        e.preventDefault();
                                        e.stopPropagation();
                                        return false;
                                    }}
                                >
                                    <span>
                                        {obligation.unit ?? UNITS["EACH"]}
                                    </span>
                                    <TriangleDownIcon className="icon"/>
                                </button>
                                <div
                                    className="dropdown-menu is-hoverable"
                                >
                                    <div
                                        className="dropdown-content"
                                    >
                                        {Object.entries(UNITS).map(([key, value]) => {
                                            return (
                                                <button
                                                    className="dropdown-item"
                                                    onClick={(e) => {
                                                        console.log("Updating");
                                                        updateObligationUnit(key)
                                                    }}
                                                >
                                                    {value}
                                                </button>
                                            );
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="media">
                            <div className="media-content">
                                <h2>Quantity</h2>
                                <p>How much or many of a resource is being requested in terms of the rate and unit of measure</p>
                            </div>
                            <div
                                className="buttons is-right"
                            >

                                <button
                                    className="button is-info"
                                    disabled={obligation.requestedUnits <= 0}
                                    onClick={lowerRequestedUnits}
                                >
                                    <DashIcon className="icon"/>
                                </button>
                                
                                <button 
                                    className="button is-info"
                                    onClick={raiseRequestedUnits}
                                >
                                    <PlusIcon className="icon"/>
                                </button>
                            </div>
                        </div>
                        <div className="media">
                            <div className="media-content">
                                <h2>Quality</h2>
                                <p>Most activities or resources have a level of quality associated with them. Ranging from meeting the fundamental aspects, to being a rare or historically significant example of a resource.</p>
                                <p>Higher qualities of resources are created and provided by individuals with higher levels of skill. The rate at which providers are rewarded correlates with the beneficiary&apos;s level of participation.</p>
                                {QUALITY_LEVELS[obligation.quality ?? 0]}
                            </div>
                            <div
                                className="buttons is-right"
                                style={{
                                    flex: "0 0 auto"
                                }}
                            >

                                <button
                                    className="button is-info"
                                    disabled={(obligation.quality ?? 0) <= 0}
                                    onClick={lowerObligationQuality}
                                >
                                    <DashIcon className="icon"/>
                                </button>
                                
                                <button 
                                    className="button is-info"
                                    disabled={obligation.quality >= QUALITY_LEVELS.length - 1}
                                    onClick={raiseObligationQuality}
                                >
                                    <PlusIcon className="icon"/>
                                </button>
                            </div>
                        </div>
                        <div className="media">
                            <div className="media-content">
                                <h2>Preference</h2>
                                <p>When more than one activity is desired to be performed, preference allows the beneficiary of an activity to express their preference for an order or priority for that activity&apos;s performance.</p>
                                {obligation.preference ?? 0}
                            </div>
                            <div
                                className="buttons is-right"
                                style={{
                                    flex: "0 0 auto"
                                }}
                            >

                    :             <button
                                    className="button is-info"
                                    disabled={allocatedPreference <= 0 || obligation.preference <= 0}
                                    onClick={lowerObligationPreference}
                                >
                                    <DashIcon className="icon"/>
                                </button>
                                
                                <button 
                                    className="button is-info"
                                    disabled={allocatedPreference >= availablePreference}
                                    onClick={raiseObligationPreference}
                                >
                                    <PlusIcon className="icon"/>
                                </button>
                            </div>
                        </div>

                        <div className="media">
                            <div className="media-content">
                                <h2>Criteria</h2>
                                <div className="message is-info">
                                    <div className="message-body">
                                        <p>
                                            Criteria enables detailed specification of how success or faliure of activity performance is evaluated.
                                        </p>
                                        <p>
                                            When activities described in an obligation has been performed, the performer may change the status to performance completed, and the party responsible for accepting the performance of the activity will either accept the performance or will describe how the performance needs improvement. If the critera have been met, the obligations&apos; will change to  accepted. If there is additional work to be completed, then the obligated parties will be notified and the obligation will change to pending performance.
                                        </p>
                                    </div>
                                </div>
                                <ol>
                                    {criteria.map((criterion) => {
                                        return (
                                            <li
                                                style={{
                                                    listStylePosition: "inside",
                                                }}
                                            >
                                                <textarea
                                                    className="textarea"
                                                    style={{
                                                        display: "inline-block"
                                                    }}
                                                    value={criterion.content}
                                                />
                                            </li>);
                                    })}
                                </ol>
                                <div className="buttons is-right">
                                    <button
                                        className="button is-info"
                                        onClick={() => {
                                            setCriteria([...criteria, {}])
                                        }}

                                    >Add Additional Criteria</button>
                                </div>
                            </div>

                        </div>

                        <div className="media">
                            <div className="media-content">
                                <h2>Promisors</h2>
                                <div className="message is-info">
                                    <div className="message-body">
                                        <p>
                                            Agreements may define more than one obligation for more than one or different parties in the agreement. To assign specific parties responsibilities or roles for a specific obligation they can be specified per obligation. Each party will be expected to meet the obligation in full, and a total of the resources involved will be updated in the summary for the obligation.
                                        </p>
                                        <p>
                                            If there are differences in the criteria or other metrics for how the activity is to be completed that are different for each entity, then the obligation should be split up, and separate obligations should be used to describe those differences. 
                                        </p>
                                    </div>
                                </div>
                                <div className="buttons is-right">
                                    <button
                                        className="button is-warning"
                                        title="Split this obligation per promisor"
                                    >
                                        Split
                                    </button>
                                </div>
                                {allParties.map((party) => {
                                    return (
                                        <div className="media"
                                        >
                                            <div className="media-content"
                                                style={{
                                                    display: "flex",
                                                    flexDirection: "row",
                                                    alignItems: "center"
                                                }}
                                            >
                                                <h3
                                                    style={{
                                                        flex: "1 0 auto"
                                                    }}
                                                >
                                                    {party.name}
                                                </h3>
                                                <button className="button is-light is-danger">
                                                    Remove 
                                                </button>
                                            </div>
                                        </div>
                                    ); 
                                })}
                                {selectedParties.map((party) => {
                                    return (
                                        <div className="media"
                                        >
                                            <div className="media-content"
                                            >
                                                <h3>{party.name}</h3>
                                            </div>
                                        </div>
                                    ); 
                                })}
                            </div>
                        </div>

            </>)
        }

        <div
            className="buttons is-right"
            style={{
                gap: "10px"
            }}
        >
            {typeof save === "function"
                ? (
                    <button
                        className="button is-success is-right"
                        disabled={!obligationModified}
                        onClick={save}
                    >
                        Save Changes
                    </button>
                ) : ""
            }
        </div>
            </div>
        </div>

        {(debug ?? false) && (<pre>{JSON.stringify(activities, null, 2)}</pre>)}
    </>);

}
