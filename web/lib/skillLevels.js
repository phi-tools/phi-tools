export const SKILL_LEVELS = [
    "UNSKILLED",
    "FRESHMAN",
    "SOPHOMORE",
    "JUNIOR",
    "SENIOR",
    "EXPERT",
    "MASTER"
];

