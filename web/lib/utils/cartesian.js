export const cartesian = ([ ...vectors ]) => {
    return vectors.reduce((partials, vector) => {
        return partials.flatMap(partial => {
            return vector.map(element => [ ...partial, element ]);
        });
    }, [[]]);
};

