export const defined = (value) => typeof value !== "undefined";
