import xs from "xstream";

import { defineElement } from "../utils/defineElement.js";

import Component from "./Component.js";

export default defineElement(
    class NotFound extends Component {
        static create() {
            const element = document.createElement("phi-not-found");
            return element;
        };
        constructor() {
            super();
            this.hideSidebar = true;
            this.connected = xs.fromObservable({
                subscribe: (listener) => {
                    this.addEventListener("CONNECTED", () => {
                        listener.next({t: "CONNECTED"});
                    });
                    return {
                        unsubscribe: () => {}
                    };
                },
            });
        }
        connectedCallback() {
            this.innerHTML = `
                <i class="fas fa-warn"></i>
                <h1>Hmmm, looks like something went wrong!</h1>
                <p>
                    Try navigating to a different page.
                </p>
            `;
            this.dispatchEvent(new Event("CONNECTED"));
        }
    }
);

