import xs from "xstream";
import marked from "marked";
import { validate as validateUuid } from "uuid";

import { defineElement } from "@phi/elements/utils/defineElement.js";

import { create as Loading } from "./phi-loading.js";
import { create as NotFound } from "./phi-not-found.js";
import { NodeDetail } from "./phi-node-detail.js";

export const Component = defineElement(class DocumentDetail extends NodeDetail {
    constructor() {
        super();
        this._loading = true;
        this.hideSidebar = false;
        this.connected = xs.fromObservable({
            subscribe: (listener) => {
                console.log("Connecting Listener");
                this.addEventListener("CONNECTED", () => {
                    console.log("GOT CONNECTED");
                    listener.next({t: "CONNECTED"});
                });
                return {
                    unsubscribe: () => {}
                };
            }
        });
        this._dataUpdated = xs.fromObservable({
            subscribe: (listener) => {
                this.addEventListener("SAVE", () => {
                    listener.next({
                        t: "DOCUMENT",
                        value: {
                            id: this._documentId,
                            name: this._heading.innerHTML,
                            content: this._contentData
                        }
                    });
                });
                return {
                    unsubscribe: () => {}
                };
            }
        });
        this._heading = document.createElement("h1");
        this._contentEditable = true;
        this._contentWrapper = document.createElement("div");
        this._contentWrapper.classList.add("phi-document-content");
        this._contentEditorTA = document.createElement("textarea");
        this._contentEditorTA.setAttribute("placeholder", "Enter document content here. Markdown syntax is supported.");
        this._contentEditorTA.classList.add("textarea");
        this._actions = document.createElement("div");
        this._actions.classList.add("actions");
        this._save = document.createElement("a");
        this._save.innerHTML = "Save";
        this._selectAgreementType = document.createElement("select");
        this._actions.replaceChildren(this._save);
        this._content = Loading();
    }

    static create({
        doc
    }) {
        const el = super.create();
        if (typeof doc !== "undefined") {
            el.setData(doc);
        }
        return el;
    }

    connectedCallback() {
        console.log("WILL SEND CONNECTED");
        this._contentEditorTA.addEventListener("input", () => {
            console.log("HELLO CHANGE");
            this._contentMd = this._contentEditorTA.value;
            this._content.innerHTML = marked(this._contentMd);
        });
        if (this._loading) {
            this._content = Loading();
            this._contentWrapper.replaceChildren(this._content);
            this.replaceChildren(this._contentWrapper);
            this.dispatchEvent(new Event("CONNECTED"));
        } else {
            this._content = document.createElement("div");
            this._contentEditorTA.value = this._contentMd;
            this._contentWrapper.replaceChildren(this._content, this._contentEditorTA);
            this._content.innerHTML = marked(this._contentMd);
            this._save.addEventListener("click", () => {
                this.dispatchEvent(new Event("SAVE"));
            });
            this.replaceChildren(this._heading, this._contentWrapper, this._actions);
            this.dispatchEvent(new Event("CONNECTED"));
        }
    }
    get _documentId() {
        if (this._data.document === null) {
            return undefined;
        }
        if (validateUuid(this._data.document.id)) {
            return this._data.document.id;
        }
        return undefined;
    }
    get _contentMd() {
        if (this._data.document === null) {
            return "";
        }
        if (typeof this._data.document.content === "string") {
            return this._data.document.content;
        }
        return "";
    }
    set _contentMd(content) {
        if (this._data.document === null) {
            this._data.document = {};
        }
        if (typeof content === "string") {
            this._data.document.content = content;
        } else {
            this._data.document.content = "";
        }
    }
    get _name() {
        if (this._data.document === null) {
            return "";
        }
        if (typeof this._data.document.name === "string") {
            return this._data.document.name;
        }
        return undefined;
    }
    setData({ data }) {
        try {
            this._data = data;
            if (this._data.document === null) {
                this._data.document = {};
            }
            this._contentEditorTA.value = this._contentMd;
            this._content = document.createElement("div");
            this._content.classList.add("content-preview");
            this._contentWrapper.replaceChildren(this._content, this._contentEditorTA);
            this._content.innerHTML = marked(this._contentMd);
            if (this._loading) {
                this._loading = false;
                this.replaceChildren(this._heading, this._contentWrapper, this._actions);
                this._save.addEventListener("click", () => {
                    console.log("WILL DISPATCH SAVE");
                    this.dispatchEvent(new Event("SAVE"));
                });
            }
        } catch(err) {
            console.error(err);
            this.replaceChildren(NotFound());
            if (this._loading) {
                this._loading = false;
            }
        }
    }
    get dataUpdated() {
        return this._dataUpdated;
    }
});

