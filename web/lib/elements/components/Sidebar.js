import { defineElement } from "../utils/defineElement.js";
import Component from "./Component.js";

export default defineElement(
    class Sidebar extends Component {
        static create({ visibility }) {
            console.log(this.elementName)
            const element = document.createElement(this.elementName);
            element.classList.add(visibility);
            return element;
        }
        constructor() {
            super();
        }
        connectedCallback() {
            this.innerHTML = `
                <phi-toggle-sidebar>
            `;
        }
        routeUpdated() {
            if (name === "landing") {
                this.classList.add("hidden");
            }
            this.classList.remove("hidden");
        }
        hide() {
            this.classList.remove("expanded");
            this.classList.add("hidden");
        }
        show() {
            this.classList.remove("hidden");
            this.classList.add("expanded");
        }
    }
);
