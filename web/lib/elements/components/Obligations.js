import xs from "xstream";

import { create as Table } from "./phi-table.js";

customElements.define(
    "phi-obligations",
    class extends HTMLElement {
        constructor() {
            super();
            this.connected = xs.fromObservable({
                subscribe: (listener) => {
                    this.addEventListener("CONNECTED", () => {
                        listener.next({t: "CONNECTED"});
                    });
                    return {
                        unsubscribe: () => {}
                    };
                },
            });
            this._heading = document.createElement("h1");
            this._heading.innerHTML = "Obligation";
            this._listing = Table({
                columns: [{
                    heading: "ID",
                    formatRecord: (record) => record.id 
                }]
            });
            console.log();
        }
        setData(data) {
            console.log(data);
            this._listing.setData([{
                id: "a"
            }, {
                id: "b"
            }]);
        }
        connectedCallback() {
            this.append(this._heading, this._listing);
            this.dispatchEvent(new Event("CONNECTED"));
        }
    }
);

export const create = () => {
    const element = document.createElement("phi-obligations");
    return element;
};

