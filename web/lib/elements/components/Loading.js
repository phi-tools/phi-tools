import xs from "xstream";

import { defineElement } from "@phi/elements/utils/defineElement.js";

import Component from "./Component.js";

export default defineElement(class Loading extends Component {
    static create() {
        console.log(this.elementName);
        const element = document.createElement(this.elementName);
        console.log(element);
        return element;
    }
    constructor() {
        super();
        this.connected = xs.fromObservable({
            subscribe: (listener) => {
                const eventHandler = () => {
                    console.log("GOT CONNECTED");
                    listener.next({
                        t: "CONNECTED",
                        source: this
                    });
                };
                this.addEventListener("CONNECTED", eventHandler);
                return {
                    unsubscribe: () => {
                        this.removeEventListener("CONNECTED", eventHandler);
                    }
                };
            },
        });//.compose(dropRepeats((x, y) => {
        //    console.log(x.source, y.source);
        //    return x.source === y.source;
        //}));
    }
    connectedCallback() {
        this.innerHTML = `
            <svg
                version="1.1"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                x="0px"
                y="0px"
                width="40px"
                viewBox="0 0 50 50"
                style="
                    enable-background: new 0 0 50 50;
                    box-sizing: border-box;
                    margin: auto;
                    width: 100%;
                    height: 75px;
                    padding: 15px;
                "
                xml:space="preserve"
            >
                <path
                    fill="#0090a8"
                    d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z"
                >
                    <animateTransform
                        attributeType="xml"
                        attributeName="transform"
                        type="rotate"
                        from="0 25 25"
                        to="360 25 25"
                        dur="0.6s"
                        repeatCount="indefinite"
                    />
                </path>
            </svg>
        `;
        this.dispatchEvent(new Event("CONNECTED"));
    }
});

