import xs, { Stream } from "xstream";
import { dasherize } from "inflected";

import { Extensible } from "../../types/Extensible.js";
import { defineElement } from "../utils/defineElement.js";

import StreamListener from "../StreamListener.js";


const CUSTOM_ELEMENT_PREFIX = "phi";

export default defineElement(
    class Component extends StreamListener {
        static get elementName() {
            return `${CUSTOM_ELEMENT_PREFIX}-${dasherize(this.name).toLowerCase()}`;
        }
    }
);
