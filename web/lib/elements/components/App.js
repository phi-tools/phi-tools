import fromEvent from "xstream/extra/fromEvent";
import { camelize } from "inflected";

import { defineElement } from "../utils/defineElement.js";
import Nav from "./Nav.js";
import NavItem from "./NavItem.js";
import Sidebar from  "./Sidebar.js";
import Footer from "./Footer.js";
import NotFound from "./NotFound.js";
import Component from "./Component.js";
import Loading from "./Loading.js";

import "@fortawesome/fontawesome-free/js/all.js";




export default defineElement(class App extends Component {
    static create({
        navigation,
        defaultContentComponent = Loading
    }) {
        console.log(this.elementName)
        const element = document.createElement(this.elementName);
        console.log(element);
        element.nav = navigation;
        element.setContentComponent({component: defaultContentComponent});
        return element;
    }
    constructor() {
        super();
        this._sidebar = Sidebar.create({ visibility: "hidden" });
        this._contentWrapper = document.createElement("div");
        this._contentWrapper.classList.add("phi-content");
        this._footer = Footer.create();
        this.connected = fromEvent(this, "CONNECTED");
        this.contentSet = fromEvent(this, "CONTENT_SET");
    }
    async connectedCallback() {
        this._contentWrapper.replaceChildren(this._content);
        this.replaceChildren(this._nav, this._sidebar, this._contentWrapper, this._footer);
        this.dispatchEvent(new Event("CONNECTED"));
    }
    async setContentComponent({ name, params, component }) {
        let Cmpnt;
        console.log({
            name,
            params,
            component
        })
        try {
            if (typeof component === "undefined") {
                console.log(name);
                const componentName = camelize(name.replace(/\./g, "-"));
                console.log(componentName);
                const componentModule = import(`@${"phi"}/elements/components/${componentName}.js`);
                Cmpnt = (await componentModule).Component;
                console.log({ params });
            }
            if (typeof component === "function") {
                if (component.prototype instanceof Component) {
                    Cmpnt = component;
                } else {
                    Cmpnt = { create: component };
                }
            }
            if (typeof component === "string") {
                Cmpnt = { create: () => document.createElement(component) };
            }
            console.log(typeof component);
            console.log(component.prototype instanceof Component);
            console.log(Cmpnt);
            console.log(Cmpnt.create);
            this._content = Cmpnt.create({...params});
            // Dispatch the event before connecting to the dom so that listeners can start 
            // paying attention to connected events
            console.log("sending content set");
            this.dispatchEvent(new Event("CONTENT_SET", this._content));
            // Attach the component which will asynchronously send the connected event.
            this._contentWrapper.replaceChildren(this._content);
            console.log("SENDING CONTENT SET");
        } catch(error) {
            this._content = NotFound.create({...params});
            this._contentWrapper.replaceChildren(this._content);
            this._sidebar.hide();
            console.error(error);
        }
    }
    set nav({ brand, start, end }) {
        this._nav = Nav.create({
            brand: NavItem.create(brand),
            start: start.map(NavItem.create),
            end: end.map(NavItem.create)
        });
    }
});
