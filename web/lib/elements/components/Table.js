import { dasherize } from "inflected";


customElements.define(
    "phi-table",
    class extends HTMLElement {
        constructor() {
            super();
            this._columns = [];
            this._data = [];
            this._table = document.createElement("table");
        }
        connectedCallback() {
            this.renderTable();
            this.append(this._table);
        }
        setColumns(columns) {
            this._columns = columns;
        }
        setData(data) {
            console.log(data);
            this._data = data;
            if (this.isConnected) {
                this.renderTable();
            }
        }
        renderTable() {
            const thead = document.createElement("thead");
            const theadr = document.createElement("tr");
            theadr.append(...this._columns.map(column => {
                const cell = document.createElement("td");
                cell.classList.add(dasherize(column.heading.toLowerCase()));
                cell.innerHTML = column.heading;
                return cell;
            }));
            thead.replaceChildren(theadr);
            const colgroup = document.createElement("colgroup");
            const cols = this._columns.map(column => {
                const col = document.createElement("col");
                col.setAttribute("span", "1");
                col.classList.add(dasherize(column.heading.toLowerCase()));
                return col;
            });
            colgroup.replaceChildren(...cols);

            const tbody = document.createElement("tbody");
            const rows = this._data.map((record) => {
                const row = document.createElement("tr");
                const cells = this._columns.map((column) => {
                    const cell = document.createElement("td");
                    cell.classList.add(dasherize(column.heading.toLowerCase()));
                    const formatted = column.formatRecord(record);
                    if (typeof formatted === "string") {
                        cell.innerHTML = formatted;
                    } else {
                        cell.replaceChildren(formatted);
                    }
                    return cell;
                });
                row.append(...cells);
                return row;
            }); 
            tbody.replaceChildren(...rows);
            this._table.replaceChildren(colgroup, thead, tbody);
        }
    }
);

export const create = ({columns, defaultData}) => {
    const element = document.createElement("phi-table");
    if (Array.isArray(defaultData)) {
        element.setData(defaultData);
    }
    element.setColumns(columns);
    return element;
};
