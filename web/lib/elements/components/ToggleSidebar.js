
customElements.define(
    "phi-toggle-sidebar",
    class extends HTMLElement {
        constructor() {
            super();

            this.addEventListener("click", () => {
                this.classList.toggle("collapse");
            });
        }
        connectedCallback() {
            this.innerHTML = `
                <i class="fas fa-user"></i>
            `;
        }
    }
);
