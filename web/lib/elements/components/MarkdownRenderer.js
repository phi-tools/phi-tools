import marked from "marked";

import { defineElement } from "@phi/elements/utils/defineElement.js";
import Component from "./Component.js";

export default defineElement(class MarkdownRenderer extends Component {
    static create({ value }) {
        const el = super.create();
        el.streams.value = value;
        return el;
    }
    get listeners() {
        return {
            md: {
                next: (value) => {
                    this.innerHTML = marked(value);
                }
            }
        };
    }
});
