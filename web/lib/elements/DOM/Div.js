HTMLDivElement.create = function({
    classList,
    innerHTML,
    children
}) {
    const element = document.createElement("div");
    element.classList.add(...classList);
    element.innerHTML = innerHTML;
    element.replaceChildren(children);
    return element;
};

export default HTMLDivElement;
