
export default class H1 {
    static create({
        innerHTML
    }) {
        const element = document.createElement("H1");
        element.innerHTML = innerHTML;
        return element;
    }
}
