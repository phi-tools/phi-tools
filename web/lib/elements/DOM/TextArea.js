export const TextArea = ({
    placeholder,
    classes
}) => {
    const element = document.createElement("textarea");
    element.setAttribute("placeholder", placeholder);
    element.classList.add(...classes);
    return element;
};

