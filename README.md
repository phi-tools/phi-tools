# Phi-Tools

Phi Tools is a software package that provides a platform to declare needs and capabilities, match the needs and cpabilities, come to agreements to meet those needs, and facilitate the meetnig of those needs through identifying preferences and critically important needs.


## Requirements

- Text Editor
- Docker

## Setting up the Matrix Service

See dendrite setup instructions for config.

we're doing just the monolith mode for right now.

https://github.com/matrix-org/dendrite/blob/master/docs/INSTALL.md


## Starting the services

`docker-compose up --build`


## Run Migrations and Seeds

`docker-compose npx knex mirgrate:latest`
`docker-compose npx knex seed:run`

