# routing.tf
resource "aws_route_table" "public" {
    vpc_id            = aws_vpc.vpc.id
    tags = {
        Name            = "PCDU Pre-Prod Route Table"
        ApplicationName = "PCDU"
        BusinessOwner   = "Medicaid Transformation"
        Division        = "DHB"
        Environment     = "Pre-Prod"
        OperationOwner  = "PCDU"
    }
} 
resource "aws_route" "public" {
    route_table_id         = aws_route_table.public.id
    destination_cidr_block = "0.0.0.0/0"
    gateway_id             = aws_internet_gateway.igw.id
}
resource "aws_route_table_association" "public" {
    count          = length(var.availability_zones.names)
    subnet_id      = element(aws_subnet.public.*.id, count.index)
    route_table_id = aws_route_table.public.id
}

