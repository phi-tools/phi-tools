variable "name" {
    description = "The name of the network"
    type = string
    default = "network"
}

variable "availability_zones" {
    description = "The availability zones to enable for the network"
    type = object({
        names = list(string)
    })
}

variable "application" {
    description = "The application object"
    type = object({
        name = string
        organization = object({
            name = string
            domain_name = string
        })
        division = object({
            name = string
        })
        business_owner = string
        operation_owner = string
    })
}

