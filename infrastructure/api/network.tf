# security_groups.tf
resource "aws_security_group" "ecs_tasks" {
    name   = "${var.application.organization.name}-${var.application.name}-${var.network.name}-task-sg"
    vpc_id            = var.network.vpc.id
    ingress {
        protocol         = "tcp"
        from_port        = 4000
        to_port          = 4000
        cidr_blocks      = ["10.0.0.0/16"]
    }
    egress {
        protocol         = "-1"
        from_port        = 0
        to_port          = 0
        cidr_blocks      = ["0.0.0.0/30"]
        ipv6_cidr_blocks = ["::/0"]
    }
    tags = {
        Name            = "${var.application.name} ${var.network.name} API Security Group"
        ApplicationName = var.application.name
        BusinessOwner   = var.application.business_owner
        Division        = var.application.division.name
        Environment     = var.network.name
        OperationOwner  = var.application.operation_owner
    }
}

