# logs.tf

# Set up CloudWatch group and log stream and retain logs for 30 days
resource "aws_cloudwatch_log_group" "dhhs_pcdu_preprod_log_group" {
    name              = "/ecs/pcdu_dev_test"
    retention_in_days = 30

    tags = {
        Name = "PCDU_Log_Group"
    }
}
resource "aws_cloudwatch_log_stream" "cb_log_stream" {
    name           = "pcdu-preprod-dev-log-stream"
    log_group_name = aws_cloudwatch_log_group.dhhs_pcdu_preprod_log_group.name
}
