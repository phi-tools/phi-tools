variable "application" {
    description = "The application object"
    type = object({
        name = string
        organization = object({
            name = string
        })
        division = object({
            name = string
        })
        business_owner = string
        operation_owner = string
    })
}

variable "cluster" {
    description = "The cluster in which to run the environment"
    type = object({
        id = string
    })
}

variable "network" {
    description = "the dns_namespace"
    type = object({
        dns_namespace = object({
            id = string
        })
    })
}

variable "min_count" {
    description = "The minimum number of services"
    type = number
    default = 1
}

variable "max_count" {
    description = "The maximum number of services"
    type = number
    default = 2
}
