


# data sources
data "aws_availability_zones" "available" {
    filter {
        name = "zone-name"
        values = [
            "us-east-1a",
            "us-east-1b",
            "us-east-1c"
        ]
    }
}

data "aws_ec2_transit_gateway" "tgw1" {
    id = "tgw-010d7fa3973aab79e"
}

data "aws_iam_policy" "pcdu_permissions_boundary" {
    arn = "arn:aws:iam::484737932465:policy/PCDU_Permissions_Boundary"
}

locals {
    availability_zones = data.aws_availability_zones.available
}

#// vpc attachment
#resource "aws_ec2_transit_gateway_vpc_attachment" "dhhs_pcdu_dev_test_tgw_attachment" {
#  subnet_ids         = aws_subnet.public.*.id
#  transit_gateway_id = data.aws_ec2_transit_gateway.tgw1.id
#  vpc_id             = aws_vpc.pcdu_dev_test.id
#  tags               = {
#    Name = "PCDU TGW"
#    Side = "Spoke"
#  }
#}

module "pcdu_application" {
    source = "./application"
    name = "pcdu"
    organization_name = "dhhs"
    organization_domain_name = "nc.gov"
    division = "dhb"
    business_owner = "Medicaid Transformation"
    operation_owner = "pcdu"
    permissions_boundary = data.aws_iam_policy.pcdu_permissions_boundary
}

module "preprod_network" {
    source = "./network"
    name = "preprod"
    application = module.pcdu_application
    availability_zones = local.availability_zones
}

#module "prod_network" {
#    source = "./network"
#    name = "pcdu-prod-network"
#    application = module.pcdu_application
#}

module "preprod_cluster" {
    source = "./cluster"
    name = "preprod"
    application = module.pcdu_application
}

#module "prod_cluster" {
#    source = "./cluster"
#    name = "prod"
#    application = module.pcdu_application
#}

module "dev_db" {
    source = "./db"
    name = "dev"
    application = module.pcdu_application
    network = module.preprod_network
    availability_zones = local.availability_zones
}

module "uat_db" {
    source = "./db"
    name = "uat"
    application = module.pcdu_application
    network = module.preprod_network
    availability_zones = local.availability_zones
}

#module "prod_db" {
#    source = "./db"
#    name = "prod"
#    application = module.pcdu_application
#    network = module.prod_network
#    availability_zones = local.availability_zones
#}

module "dev_environment" {
    name = "dev"
    branch = "develop"
    application = module.pcdu_application
    network = module.preprod_network
    cluster = module.preprod_cluster
    db = module.dev_db
}

module "uat_environment" {
    name = "uat"
    branch = "staging"
    application = module.pcdu_application
    network = module.preprod_network
    cluster = module.preprod_cluster
    db = module.uat_db
}

#module "prod_environment" {
#    name = "prod"
#    branch = "master"
#    application = module.pcdu_application
#    network = module.prod_network
#    cluster = module.prod_cluster
#    db = module.dev_db
#}

module "dev_api" {
    source = "./api"
    name = "dev"
    branch = "develop"
    application = module.pcdu_application
    network = module.preprod_network
    cluster = module.preprod_cluster
    db = module.dev_db
}

module "uat_api" {
    source = "./api"
    name = "uat"
    branch = "master"
    application = module.pcdu_application
    network = module.preprod_network
    cluster = module.preprod_cluster
    db = module.uat_db
}

#module "prod_api" {
#    source = "./api"
#    name = "prod"
#    branch = "master"
#    application = module.pcdu_application
#    network = module.prod_network
#    cluster = module.prod_cluster
#    db = module.prod_db
#}

module "preprod_gateway" {
    source = "./gateway"
    application = module.pcdu_application
    network = module.preprod_network
    cluster = module.preprod_cluster
    environments = [
        module.uat_api,
        module.dev_api
    ]
}

#module "prod_gateway" {
#    source = "./gateway"
#    application = module.pcdu_application
#    network = module.prod_network
#    cluster = module.prod_cluster
#}

