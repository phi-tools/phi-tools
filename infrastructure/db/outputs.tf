output "rds_azs" {
    value = aws_rds_cluster.db.availability_zones
}

output "endpoint" {
    value = aws_rds_cluster.db.endpoint
}
