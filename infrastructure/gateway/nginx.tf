data "template_file" "nginx_conf" {
    template = <<EOF
worker_processes 2;
worker_rlimit_nofile 20000;

events {
    worker_connections 768;
}

http {

    # Because by default nginx *really* needed to save this much memory...?
    server_names_hash_bucket_size 128;   

    # Optimizes data transfer copying data between one file descriptor and another
    # instead of reading and copying data to/from user space.
    sendfile on;

    # Causes NGINX to attempt to send its HTTP response head in one packet,
    # instead of using partial frames. This is useful for prepending headers before calling sendfile,
    # or for throughput optimization.
    tcp_nopush on;

    # Disables the Nagle algorithm.
    # It's useful for sending frequent small bursts of data in real time.
    tcp_nodelay  off;

    # Timeout during which a keep-alive client connection will stay open to serve 
    # all the requested files from the server side.
    keepalive_timeout  30s;

    # If a client does not receive anything within the time set in this directive, a connection is closed. 
    send_timeout 60s; 

    # In production you MUST set gzip to "on" in order to save bandwidth. Web browsers
    # which handle compressed files (all recent ones do) will get a very smaller version
    # of the server response. 
    gzip on;

    # Enables compression for a given HTTP request version.
    gzip_http_version 1.0;

    # Compression level 1 (fastest) to 9 (slowest).
    gzip_comp_level 6;

    # Enables compression for all proxied requests.
    gzip_proxied any;

    # Minimum length of the response (bytes). Responses shorter than this length will not be compressed.
    gzip_min_length 10000;

    # Enables compression for additional MIME-types.
    gzip_types text/plain text/css application/x-javascript text/xml application/xml application/xml+rss text/javascript;

    # Disables gzip compression for User-Agents matching the given regular expression.
    # Is this case we've disabled gzip for old versions of the IE that don't support compressed responses.
    gzip_disable "MSIE [1-6] \.";

    # You can include other config files using the "include" directive.
    # Here we are including the mime-types, for example.
    include /etc/nginx/mime.types;

    # The "application/octet-stream" means that the response is a binary file.
    # If this directive is not defined then response will be set to "text/plain" by default.
    default_type application/octet-stream;

    # Redirect people to the secure site.
    server {
        listen 80;
        server_name _;
        
        location / {
            return 301 https://$host$request_uri;
        }

        location /isitup {
            return 200;
        }
    }

    # Used for local development
    server {
        listen 443 ssl;
        server_name localhost;
        
        # Docker resolver for user defined networks
        resolver 127.0.0.11;

        auth_basic "PCDU Local Environment";
        auth_basic_user_file /etc/nginx/.htpasswd;
        
        ssl_certificate /etc/ssl/certificate.crt;
        ssl_certificate_key /etc/ssl/private.key;

        set $api pcdu;

        root /srv/pcdu/;

        location / {
            try_files $uri $uri/ /index.html;
        }

        location /graphql {
            proxy_pass https://$api:4000;
        }

        location /isitup {
            return 200;
        }

    }

    # Config for aws cloud
    server {
        listen 443 ssl;
        server_name pcdu-lb-preprod-71371d2e1479a110.elb.us-east-1.amazonaws.com;

        # AWS Service Discovery resolver is at root of CIDR block plus 2.
        # https://medium.com/driven-by-code/dynamic-dns-resolution-in-nginx-22133c22e3ab
        resolver 10.0.0.2;

        auth_basic "PCDU Preproduction Environment";
        auth_basic_user_file /etc/nginx/.htpasswd;
        
        ssl_certificate /etc/ssl/certificate.crt;
        ssl_certificate_key /etc/ssl/private.key;
        
        set $api api.preprod.pcdu.dhhs.nc.gov.local;

        root /srv/pcdu/;

        location / {
            try_files $uri $uri/ /index.html;
        }

        location /graphql {
            proxy_pass https://$api:4000;
        }

        location /isitup {
            return 200;
        }

    }
}
EOF

    vars {
        servers      = "${join("\n        ", data.template_file.nginx_server_node.*.rendered)}"
        servers_http = "${join("\n        ", data.template_file.nginx_server_node_http.*.rendered)}"
    }
}

resource "aws_s3_bucket" "my_bucket" {
    bucket = "my_bucket_name"

    versioning {
        enabled = true
    }
}

resource "aws_s3_bucket_object" "file_upload" {
    bucket = "my_bucket"
    key    = "my_bucket_key"
    source = "${path.module}/my_files.zip"
    etag   = "${filemd5("${path.module}/my_files.zip")}"
}

