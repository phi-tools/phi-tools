# Task Execution Role
resource "aws_iam_role" "ecs_task_execution_role" {
    name = "PCDU_ecsTaskExecutionRole"
    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "ecs-tasks.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
    permissions_boundary = var.permissions_boundary.arn
    tags = {
        Name            = "PCDU Pre-Prod API ECS Task Execution Role"
        ApplicationName = "PCDU"
        BusinessOwner   = "Medicaid Transformation"
        Division        = "DHB"
        Environment     = "Pre-Prod"
        OperationOwner  = "PCDU"
    }
}
resource "aws_iam_role_policy_attachment" "ecs-task-execution-role-policy-attachment" {
    role       = aws_iam_role.ecs_task_execution_role.name
    policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

# Task Role
data "aws_iam_policy_document" "dhhs_pcdu_ecs_task_policy_document" {
    statement {
        sid = "dhhsPcduEcsTaskPermissions"
        actions = [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents",
            "logs:DescribeLogStreams"
        ]
        resources = [
            "arn:aws:logs:::*"
        ]
    }
}
resource "aws_iam_policy" "dhhs_pcdu_ecs_task_policy" {
    name   = "PCDU_EcsTaskPolicy"
    path   = "/"
    policy = data.aws_iam_policy_document.dhhs_pcdu_ecs_task_policy_document.json
}

resource "aws_iam_role" "ecs_task_role" {
    name = "PCDU_ecsTaskRole"
    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "ecs-tasks.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
    permissions_boundary = var.permissions_boundary.arn
    tags = {
        Name            = "PCDU Pre-Prod API ECS Task Role"
        ApplicationName = "PCDU"
        BusinessOwner   = "Medicaid Transformation"
        Division        = "DHB"
        Environment     = "Pre-Prod"
        OperationOwner  = "PCDU"
    }
}
resource "aws_iam_role_policy_attachment" "ecs-task-role-policy-attachment" {
    role       = aws_iam_role.ecs_task_execution_role.name
    policy_arn = aws_iam_policy.dhhs_pcdu_ecs_task_policy.arn
}

