variable "name" {
    description = "The name of the application"
    type = string
    default = "app"
}

variable "organization_name" {
    description = "The name of the organization operating the application"
    type = string
}

variable "organization_domain_name" {
    description = "The tld that this organization's applications operate in"
    type = string
}

variable "division" {
    description = "The business division that operates the application"
    type = string
    default = "division"
}

variable "business_owner" {
    description = "The owner of the application business"
    type = string
    default = "Business Owner"
}

variable "operation_owner" {
    description = "The owner of the application operation"
    type = string
    default = "Operation Owner"
}

variable "permissions_boundary" {
    description = "The permissions boundary for the application execution"
    type = object({
        arn = string
    })
}
