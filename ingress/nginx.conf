events {
    worker_connections 768;
}

http {

    server_names_hash_bucket_size 128;   
    sendfile on;
    tcp_nopush on;
    tcp_nodelay  off;
    keepalive_timeout  30s;
    send_timeout 60s; 
    gzip on;
    gzip_http_version 1.0;
    gzip_comp_level 6;
    gzip_proxied any;
    gzip_min_length 10000;
    gzip_types text/plain text/css application/x-javascript text/xml application/xml application/xml+rss text/javascript;
    gzip_disable "MSIE [1-6] \.";

    include /etc/nginx/mime.types;

    default_type application/octet-stream;

    # Redirect unsecure connections.
    server {
        listen 80;
        server_name _;
        
        location / {
            return 301 https://$host$request_uri;
        }

        location /isitup {
            return 200;
        }
    }

    # Local Development
    server {
        listen 443 ssl;
        server_name localhost;
        
        # Docker resolver for user defined networks
        resolver 127.0.0.11;

        ssl_certificate /etc/ssl/certs/certificate.pem;
        ssl_certificate_key /etc/ssl/private/private.pem;

        set $web web;
        set $api api;

        location /matrix {
            proxy_set_header host $http_host;
            proxy_pass https://$api:9000;
        }

        location /graphql {
            proxy_set_header host $http_host;
            proxy_pass https://$api:9000;
        }

        location / {
            proxy_set_header host $http_host;
            proxy_pass https://$web:8080;
        }

    }

    # Config for aws cloud
    server {
        listen 443 ssl;
        server_name phi.tools phi-tools-lb-preprod-5fd080a42accc4f4.elb.us-east-1.amazonaws.com;

        # AWS Service Discovery resolver is at root of CIDR block plus 2.
        # https://medium.com/driven-by-code/dynamic-dns-resolution-in-nginx-22133c22e3ab
        resolver 10.0.0.2;

        auth_basic "Pre-Prod Environment";
        auth_basic_user_file /etc/nginx/.htpasswd;
        
        ssl_certificate /etc/ssl/certs/certificate.pem;
        ssl_certificate_key /etc/ssl/private/private.pem;
        
        set $app  app.preprod.phi.local;

        location /graphql {
            proxy_set_header host $http_host;
            proxy_pass https://$app:4000;
        }

        location / {
            proxy_set_header host $http_host;
            proxy_pass https://$app:4000;
        }

    }
}
